<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.

 Portfoliotheme functions and definitions
 * @package Portfolio
 * @author Purplethemes
 */
?>
<?php global $portfolio_options; 

$fb_url = $portfolio_options['facebook_url'];
$twtr_url = $portfolio_options['twitter_url'];
$gplus_url = $portfolio_options['googleplus_url'];
$linkin_url = $portfolio_options['linkedin_url'];
$rss_url = $portfolio_options['blogger_url'];
$ftr_text = $portfolio_options['footer_text'];

?>
		<!-- </div>#main -->
<!-- footer section -->
    <footer>

        <section class="footer-navigation">
            <div class="container">
                <section class="row">
                    <article class="col-xs-12 col-sm-6 col-md-3">

                        <!-- footer social section -->
                        <div class="social-follow wow fadeInUp">
                            <h5 class="text-center">Follow Us On</h5>
                            <ul class="socials">
                            
                                <li><a class="ir" target="_blank" href="<?php echo $portfolio_options['twitter_url']?$portfolio_options['twitter_url']:"#"; ?>" title="Share on Twitter" ><i class="fa fa-twitter"></i></a></li>    
                               
                                <li><a class="ir" target="_blank" href="<?php echo $portfolio_options['facebook_url']?$portfolio_options['facebook_url']: "#" ; ?>" title="Share on Facebook" ><i class="fa fa-facebook"></i></a></li>
                            
                                <li><a class="ir" target="_blank" href="<?php echo $portfolio_options['googleplus_url']?$portfolio_options['googleplus_url']: "#"; ?>" title="Share on Google Plus" ><i class="fa fa-google-plus"></i></a></li>
                                <li><a class="ir" target="_blank" href="<?php echo $portfolio_options['linkedin_url']?$portfolio_options['linkedin_url']: "#"; ?>" title="Share on LinkedIn" ><i class="fa fa-linkedin"></i></a></li>
                           
                               <li><a class="ir" target="_blank" href="<?php echo $portfolio_options['blogger_url']?$portfolio_options['blogger_url']: "#"; ?>" title="Share on RSS" ><i class="fa fa-rss"></i></a></li>
                            
                               
                            </ul>
                        </div>
                        <!-- footer social section end -->

                    </article>
                    <article class="col-xs-12 col-sm-6 col-md-3">

                        <!-- newsletter section -->
                        <div class="newsletter wow fadeInUp" data-wow-delay="0.3s">
                            <h5 class="text-center">Newsletter</h5>
                            <form id="newsletter_form">
                                <div class="subscribe-block">
                                    <div class="input-box">
                                        <input type="email" name="email" id="newsletter" class="input-text" placeholder="Email Address" required autocomplete="off">
                                    </div>
                                    <div class="actions">
                                        <button type="submit" title="Go" class="button"><span><span><i class="fa fa-check"></i></span></span>
                                        </button>
                                        
                                   </div>
                                </div>
                            </form>
                        </div>
                        <!-- newsletter section end -->

                    </article>
                    <article class="col-xs-12 col-sm-12 col-md-6">

                        <!-- sitemap section -->
                        <div class="sitemap-block wow fadeInUp" data-wow-delay="0.4s">
                            <?php wpt_wp_footer_menu(); ?>
                        </div>
                       

                    </article>
                     <!-- sitemap section end -->
                    <article class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <!-- copyright section -->
                        <div class="copyright"><?php if ( $ftr_text ) : ?>&copy; <?php echo $ftr_text; ?> <?php else : ?>&copy; 2014. All rights reserved <?php endif; ?>| Design by <a href="http://www.augustinfotech.com/" target="_blank">August Infotech</a>.
                        </div>
                        <!-- copyright section end -->
                    </article>
                </section>
            </div>
        </section>
    </footer>
    <!-- footer section end-->
    <?php echo portfolio_scripts(); ?>
    <!-- scroll to top section -->
    <a href="#" class="back-to-top"><i class="glyphicon glyphicon-arrow-up"></i></a> 
    <!-- scroll to top section end -->
		
</body>
</html>