<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * Portfoliotheme functions and definitions
 * @package Portfolio
 * @author Purplethemes
 */
 
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<?php global $portfolio_options; ?>
<head>
    
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <?php 
    
    $portfolio_site_title= $portfolio_options['site_title'];
     ?>
     
     <title><?php if($portfolio_site_title){ echo $portfolio_site_title; } else { echo wp_title( '|', true, 'right' ); } ?></title>   
     
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="keywords" content="Portfilo">
    <meta name="description" content="<?php echo $portfolio_options['site_desc']; ?>">
    <meta name="author" content="Portfilo">
    <meta name="robots" content="noindex,nofollow,noarchive,noodp,noydir">
    
    <!-- stylesheet -->
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" media="all" />
<?php do_action('wp_styles'); ?>



      
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    
        <!-- favicon section -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
        <?php
            
            $portfolio_favicon=$portfolio_options['favicon']['url'];
            if($portfolio_favicon)
            {
                echo '<link rel="shortcut icon" href="'.$portfolio_favicon.'">';
            }    
        
            else
            {
                echo '<link rel="shortcut icon" href="ico/favicon.png">';
            }
        ?>
        
        <link rel="icon" href="ico/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="ico/favicon.ico" type="image/x-icon" />
        <!-- favicon section end -->
    
   
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
    <?php do_action('wp_enqueue_scripts'); ?>
	<?php wp_head(); ?>
</head>

<body>
 <!-- main body section -->
 
 <?php 
			get_template_part( 'libs/header', 'top-area' );
            
            $slider = false;
            if( get_post_type()=='page' && is_front_page() ) {
            	get_template_part( 'libs/header', 'slider' ) ;
            	$slider = true;
            }	
            if($portfolio_options['hide_breadcrumbs_from_bar']== 1){
                if( ! is_404() && $slider != 1 ){  
                    if( trim( wp_title( '', false ) ) ){
                  	// Page title
                  	    echo '<section class="breadcrum-area">';
                            echo '<div class="container">';                    
                                   if( get_post_type()=='page' || is_single() ){
                                   	
    								    echo '<h2 data-wow-delay="0.7s" class="wow fadeIn animated pull-right" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeIn;">'. $post->post_title .'</h2>';
    						       } 
                                   else if( is_tax() ) {
                                   
                                         $queried_object = get_queried_object(); 
                                         $term_id = $queried_object->term_id; 
                                         $term_obj = get_term( $term_id, "portfolio_category"); 
                                         echo '<h2 data-wow-delay="0.7s" class="wow fadeIn animated pull-right" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeIn;">'. $term_obj->name .'</h2>';
                                   }
                                   else {
                                   
    								    echo '<h2 data-wow-delay="0.7s" class="wow fadeIn animated pull-right" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeIn;">'. trim( wp_title( '', false ) ) .'</h2>';
    						        }
                                
                                      	 wpt_breadcrumbs();
                                      
                            echo '</div>';
                       echo '</section>';
                    }
                }  
            }//end of if    	
?>