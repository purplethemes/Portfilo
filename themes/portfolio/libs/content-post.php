<?php
/**
 * The template for displaying blog posts.
 *
 * @package Portfolio
 * @author Purplethemes
 */

global $wp_query, $portfolio_options;
$post_id = $wp_query->get_queried_object_id();
$blog_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'blog-listing-thumbnail' ) ); 

$categories = get_the_category();
$separator = ',';
$output = '';

foreach($categories as $category) {
	$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
}

?>
 
 <figure class="news wow fadeInLeft" data-wow-delay="0.3s">
        <div class="col-xs-12 col-sm-5 col-md-5">
            <div class="news-img">
                <a href="<?php the_permalink(); ?>">
                	<?php if(!empty($blog_url)) { ?>
                    	<img class="img-responsive" src="<?php echo $blog_url; ?>" alt="News Title" />
                    <?php } else { ?>
						<img class="img-responsive" src="<?php echo THEME_URI. '/images/no-img-portfolio.jpg'; ?>" alt="News Title"  style="width:458px;height:305px;" /> 
					<?php } ?>
                </a>
            </div>
        </div>
                       
        <figure class="col-xs-12 col-sm-7 col-md-7">
            <figcaption class="news-info">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <div class="meta"> <span class="date"><i class="glyphicon glyphicon-calendar"></i><?php echo get_the_date(); ?></span>  <span class="category"><i class="glyphicon glyphicon-tag"></i>
                    <?php echo trim($output, $separator); ?></span> 
                </div>
                <?php the_content(); ?>
               
                <a class="more-news" href="<?php the_permalink(); ?>">Find Out More</a> 
            </figcaption>
        </figure>
</figure>
<div class="divider"></div>
                    
 
