<?php
/**
 * The template for displaying breadcrumbs
 *
 * @package Portfolio
 * @author Purplethemes
 */
?>

<!-- breadcrum section start -->
    <section class="breadcrum-area">
        <div class="container">
            <h2 data-wow-delay="0.7s" class="wow fadeIn animated pull-right" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeIn;">Projects 2 Columns</h2>
            <span data-wow-delay="1s" class="breadcrum pull-left wow fadeIn animated" style="visibility: visible; animation-delay: 1s; animation-name: fadeIn;"><a href="index.html"><i class="glyphicon glyphicon-home"></i></a>/&nbsp;&nbsp;<span class="active"><?php the_breadcrumb(); ?></span></span>
        </div>
    </section>
    <!-- breadcrum section end -->