<?php


global $portfolio_options; 

$layout = $portfolio_options['gallery_post_layout_select'];

switch($layout){
    case '1':
		$class = 'col-xs-6 col-sm-6 col-md-6';
        break;
	case '2':
		$class = 'col-xs-6 col-sm-4 col-md-4';
		break;
	case '3':
		$class = 'col-xs-6 col-sm-3 col-md-3';
		break;
    default :
        $class = '';
		break;
}

$gallery_listing_full_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'full');
$gallery_listing_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'gallery-listing');
$gallery_listing_imgURL = $gallery_listing_imgArray[0];
$full_title =  get_the_title();

                  
?>
            <article class="<?php echo $class; ?>">
                <div class="plan-container">
                    <div class="plan-img">
                    <?php if(!empty($gallery_listing_imgURL)) { ?>
                    	<img title="<?php echo $full_title;  ?>" alt="Plan Name" class="img-responsive" src="<?php  echo $gallery_listing_imgURL; ?>">
                    <?php } else { ?>
						<img title="<?php echo $full_title;  ?>" alt="Plan Name" class="img-responsive" src="<?php  echo THEME_URI. '/images/no-img-portfolio.jpg'; ?>" style="width:555px;height:415px;" >
					<?php } ?>
                        
                        <div class="plan-entry-hover">
                        <?php if(!empty($gallery_listing_full_imgArray)) { ?>
                            <a class="gallery" href="<?php echo $gallery_listing_full_imgArray[0]; ?>" data-lightbox="image-gallery" data-title="<?php echo $full_title; ?><br/><span class='lb-desc'><?php echo the_content(); ?></span>"><i class="glyphicon glyphicon-zoom-in"></i></a>
                        <?php } else { ?>
							<a class="gallery" href="<?php echo THEME_URI. '/images/no-img-portfolio.jpg'; ?>" data-lightbox="image-gallery" data-title="<?php echo $full_title; ?><br/><span class='lb-desc'><?php echo the_content(); ?></span>"><i class="glyphicon glyphicon-zoom-in"></i></a>
						<?php } ?>    
                            
                        </div>
                    </div>
                </div>
            </article>

   