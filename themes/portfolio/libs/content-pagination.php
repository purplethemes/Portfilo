<?php
/**
 * The template for displaying pagination 
 *
 * @package Portfolio
 * @author Purplethemes
 */
?>

<article class="col-xs-12 col-sm-12 col-md-12 text-right">
    <ul class="pagination wow fadeInUp" data-wow-delay="0.3s">
        <?php wpt_pagination(); ?>
    </ul>
</article>
 
