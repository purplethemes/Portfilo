<?php
/**
 * The template for displaying content in the template-portfolio.php template
 *
 * @package Portfolio
 * @author Purplethemes
 */

global $portfolio_options; 
$layout = $portfolio_options['portfolio_post_layout_select'];

switch($layout){
    case '1':
		$class = 'col-xs-6 col-sm-6 col-md-6 effect-apollo';
        break;
	case '2':
		$class = 'col-xs-6 col-sm-4 col-md-4 effect-apollo';
		break;
	case '3':
		$class = 'col-xs-6 col-sm-3 col-md-3 effect-apollo';
		break;
    default :
        $class = '';
		break;
}

$pf_listing_full_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'portfolio-listing');
$pf_listing_imgURL = $pf_listing_full_imgArray[0];
$full_title =  get_the_title();
$sub_title = explode(' ',$full_title, 2);                 
?>
                    
<figure class="<?php echo $class; ?>">
<?php if(!empty($pf_listing_imgURL)) { ?>
	<img alt="" src="<?php  echo $pf_listing_imgURL; ?>">
<?php } else { ?>
	<img alt="" src="<?php  echo THEME_URI. '/images/no-img-portfolio.jpg'; ?>" style="width:555px;height:auto;"> 
<?php } ?>
        <figcaption>
            <h3><?php echo $sub_title[0]; ?><span><?php echo $sub_title[1]; ?></span> </h3>
            <a href="<?php the_permalink(); ?>"><?php __('View more','wpt')?></a>
        </figcaption>
</figure>

                   