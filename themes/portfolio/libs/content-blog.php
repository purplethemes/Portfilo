<?php
/**
 * The template for displaying blog content
 *
 * @package Portfolio
 * @author Purplethemes
 */
 
global $wp_query, $portfolio_options; 

$post_id = $wp_query->get_queried_object_id(); 
$blog_detail_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'blog-detail-thumbnail' ) );  
$content_post = get_post($post_id);
$content = $content_post->post_content;

$post_id = $wp_query->get_queried_object_id();
$args=array(
  'page_id' => $post_id,
  'post_type' => 'post',
  'post_status' => 'publish',
  'posts_per_page' => 1,
  'caller_get_posts'=> 1
);
$myposts = get_posts( $args );


$categories = get_the_category();
$cnt = 0;

$separator = ',';
$output = '';
foreach($categories as $category) {
	$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
	}
	
 
foreach ( $myposts as $post ) : setup_postdata( $post ); 
?> 
<figure class="news wow fadeInLeft" data-wow-delay="0.3s">
                            <div class="news-img">
                            <?php if(!empty($blog_detail_url)) { ?>
                                <img class="img-responsive" src="<?php echo $blog_detail_url; ?>" alt="News Title" />
                                <?php } else { ?>
									<img class="img-responsive" src="<?php echo THEME_URI. '/images/no-img-portfolio.jpg'; ?>"  alt="News Title" />	
								<?php } ?> 
                            </div>
                            <figcaption class="news-info">
                                <h2><?php the_title(); ?></h2>
                                <div class="meta"><span class="date"><i class="glyphicon glyphicon-calendar"></i><a href="<?php the_permalink(); ?>"><?php echo get_the_date(); ?></a></span><span class="category"><i class="glyphicon glyphicon-tag"></i> 
                                	<?php echo trim($output, $separator); ?></span> </span> 
                                </div>
                                
                                <?php the_content(); ?>
                            </figcaption>
                        </figure>
                        <div class="news-detail-divider"></div>

                        <!-- news sharing section -->
                        <section class="social-box">
                            <div class="tweet-like"> <a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a> 
                                <script>
                                    ! function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (!d.getElementById(id)) {
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = "https://platform.twitter.com/widgets.js";
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }
                                    }(document, "script", "twitter-wjs");
                                </script>
                            </div>
                            <div class="fb-like">
                                <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FAugust.Infotech&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=21" style="border:none; overflow:hidden; height:21px;"></iframe>
                            </div>
                            <div class="pin-like">
                                <a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F&amp;media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg&amp;description=Next%20stop%3A%20Pinterest" data-pin-do="buttonPin" data-pin-config="beside">
                                    <img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" alt="Pinterest" />
                                </a>
                                <!-- Please call pinit.js only once per page -->
                                <script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>
                            </div>
                        </section>
                        <!-- news sharing section end -->
 
<?php endforeach; 
wp_reset_postdata(); 

if ($portfolio_options['hide_author_bio'] == 1) {
	echo '<h4> About the Author </h4>';
	echo the_author_description();
}

if($portfolio_options['hide_comments'] == 1) {
	// If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) {
        comments_template();
    }
}

//include( TEMPLATEPATH . '/comments.php'); ?>
