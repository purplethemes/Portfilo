<?php
/**
 * The template for displaying counter
 *
 * @package Portfolio
 * @author Purplethemes
 */
global $portfolio_options;

$cnt = 0;
for($i=1; $i<=4; $i++){
	if(!empty($portfolio_options['counter_checkbox_'.$i]))
	{
		$cnt++;	
	}
}

switch($cnt){
    case '1':
		$class = 'col-xs-12 col-sm-6 col-md-12';
        break;
	case '2':
		$class = 'col-xs-12 col-sm-6 col-md-6';
		break;
	case '3':
		$class = 'col-xs-12 col-sm-6 col-md-4';
		break;
	case '4':
		$class = 'col-xs-12 col-sm-6 col-md-3';
		break;
    default :
        $class = '';
		break;
}
?>

<section class="counter-section">
    <div class="container">
    
      <?php if ($cnt != "0") { ?>
        <section class="row" id="counter-area">
           
            <?php for($i=1; $i<=4; $i++){ 
            
	            if($portfolio_options['counter_checkbox_'.$i]) { ?>
	            
					<article class="<?php echo $class; ?>">
	                    <div class="counter-block wow fadeInUp" data-wow-delay="0.4s">
	                        <?php if($portfolio_options['counter_count_'.$i] && $portfolio_options['counter_checkbox_'.$i] ){ ?><i class="fa fa-briefcase fa-3x"></i><?php } else { echo ''; } ?>
	                        <?php if($portfolio_options['counter_count_'.$i] && $portfolio_options['counter_checkbox_'.$i] ){ ?><p class="cnt-no"><?php echo $portfolio_options['counter_count_'.$i]; ?></p><?php } else { echo ''; } ?>
	                        <?php if($portfolio_options['counter_title_'.$i] && $portfolio_options['counter_checkbox_'.$i] ){ ?><p class="cnt-title"><?php echo $portfolio_options['counter_title_'.$i]; ?></p><?php } else { echo ''; } ?>
	                    </div>
	                </article>	
				<?php }
            	
			} ?>
			
		</section>
		<?php } ?>
    </div>
</section>

