<?php
/**
 * Menu functions.
 * @package Portfolio
 * @author Purplethemes
 * 
 */

/* ---------------------------------------------------------------------------
 * Registers a menu location to use with navigation menus.
 * --------------------------------------------------------------------------- */
register_nav_menu( 'primary', __( 'Main menu' , 'wpt' ) );
register_nav_menu( 'footer', __( 'Footer menu' , 'wpt' ) );

/* ---------------------------------------------------------------------------
 * Main menu
 * --------------------------------------------------------------------------- */
function wpt_wp_nav_menu() 
{
	$args = array( 
	    'container' 	  => 'nav',
		'container_id'	  => 'menu',
		'menu_class'      => 'nav navbar-nav navbar-right',
		'fallback_cb'	  => 'wpt_wp_page_menu', 
		'theme_location'  => 'primary',
        'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
		'depth' 		  => 0,
        
	);
   
	wp_nav_menu( $args ); 
}

function wpt_wp_page_menu() 
{
	$args = array(
		'title_li' => '0',
		'sort_column' => 'menu_order',
		'depth' => 0
	);

	echo '<nav id="menu">'."\n";
		echo '<ul>'."\n";
			wp_list_pages($args); 
		echo '</ul>'."\n";
	echo '</nav>'."\n";
}

/* ---------------------------------------------------------------------------
 * Footer menu
* --------------------------------------------------------------------------- */
function wpt_wp_footer_menu() {
	
    $footer_menu = wp_get_nav_menu_object( "footer" ); 
	$menu_items = wp_get_nav_menu_items($footer_menu->term_id);
   
	$complete = '';
	foreach ( (array) $menu_items as $key => $menu_item ) {
	    $title = $menu_item->title;
	    $url = $menu_item->url;
	    
	    if( $menu_item->menu_item_parent == 0 ):
			if( $complete == 'false'):
			  $menu_list .= '</ul></div>';
			endif;
			$menu_list .= '<div class="sitemap"><h5>'.$title.'</h5><ul>';
			$complete  = 'false';
	    else: 
	      $menu_list .= '<li><a href="' . $url . '">' . $title . '</a></li>';
	    endif;  
	}
	
	echo $menu_list;
}    

?>