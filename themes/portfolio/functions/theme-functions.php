<?php
/**
 * Theme functions.
 *
 * Portfoliotheme functions and definitions
 * @package Portfolio
 * @author Purplethemes
 */


/* ---------------------------------------------------------------------------
 * Theme support
 * --------------------------------------------------------------------------- */

add_action( 'after_setup_theme', 'portfolio_theme_setup' );
function portfolio_theme_setup() {
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'portfolio-listing', 555, 415, true ); // portfolio - listing
	add_image_size( 'portfolio-single', 954, 440, true ); // portfolio - single
	add_image_size( 'portfolio-homepage', 240, 240, true ); // portfolio - homepage
	add_image_size( 'portfolio-homepagelarge', 799, 539, true ); //portfolio - homepage (large)
	
    add_image_size( 'gallery-listing', 555, 415, true ); // gallery - listing
    add_image_size( 'gallery-homepage', 240, 240, true ); // gallery - homepage
	add_image_size( 'galllery-homepagelarge', 799, 539, true ); //gallery - homepage (large)
	
	add_image_size( 'cpt-logo-thumbnail',100,100 );
	
	add_image_size( 'aboutus-thumbnail', 1140 ,640,true);
	
	add_image_size( 'blog-listing-thumbnail', 458 ,244,true);
	add_image_size( 'blog-detail-thumbnail', 750 ,400,true);
	
	add_image_size( 'testimonial-homepage', 87, 88, true ); // testimonial - homepage
    //add_image_size( 'pf-homepage', 253 ,253);
}

/********* Load Redux Options Framework ***********/
if ( !class_exists( 'ReduxFramework' ) && file_exists( LIBS_DIR. '/redux/ReduxCore/framework.php' ) ) {
	require_once( LIBS_DIR. '/redux/ReduxCore/framework.php' );
   
}
if ( !isset( $redux_demo ) && file_exists( LIBS_DIR. '/redux.php' ) ) {
	require_once( LIBS_DIR. '/redux.php' );
    
}

/***************** Add Portfolio *****************/
    require_once( LIBS_DIR. '/_portfolio.php' );
    

/***************** Add Gallery *****************/
    require_once( LIBS_DIR. '/_gallery.php' );
    
/***************** Add Slider *****************/
    require_once( LIBS_DIR. '/_slider.php' );
    
/***************** Add Testimonial *****************/
    require_once( LIBS_DIR. '/_testimonial.php' );
    
 

/***************** Pagination function *****************/    

function wpt_pagination() {
   
	global $paged, $wp_query;
	
	/*echo "<pre>";
	print_r($wp_query);*/
	$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;  
	
	if( empty( $paged ) ) $paged = 1;
	$prev = $paged - 1;							
	$next = $paged + 1;
	
	$end_size = 1;
	$mid_size = 2;
	$show_all = true;
	$dots = false;	

	if( ! $total = $wp_query->max_num_pages ) $total = 1;
	
	if( $total > 1 )
	{
		
		if( $paged >1 ){
			echo '<li><a class="prev_page" href="'. get_pagenum_link($current-1) .'">'. __('&lsaquo;','wpt') .'</a></li>';
		}

		for( $i=1; $i <= $total; $i++ ){
			if ( $i == $current ){
				echo '<li class="active">';
					echo '<a href="'. get_pagenum_link($i) .'">'. $i .'</a>&nbsp;';
				echo '</li>';
				$dots = true;
			} else {
				if ( $show_all || ( $i <= $end_size || ( $current && $i >= $current - $mid_size && $i <= $current + $mid_size ) || $i > $total - $end_size ) ){
					echo '<li>';
					   echo '<a href="'. get_pagenum_link($i) .'">'. $i .'</a>&nbsp;';
				    echo '</li>';
					$dots = true;
				} elseif ( $dots && ! $show_all ) {
					echo '<span class="page">...</span>&nbsp;';
					$dots = false;
				}
			}
		}
		
		if( $paged < $total ){
			echo '<li><a class="next_page" href="'. get_pagenum_link($paged+1) .'">'. __('&rsaquo;','wpt') .'</a></li>';
		}

	}	
}



/***************** Breadcrumbs function *****************/  
  
function wpt_breadcrumbs() {

global $post;

$homeLink = get_bloginfo('url');
$showCurrent = 1;
	
    echo '<span data-wow-delay="1s" class="breadcrum pull-left wow fadeIn animated" style="visibility: visible; animation-delay: 1s; animation-name: fadeIn;">';
	    echo '<ul>';
        	echo '<li><a href="'. $homeLink .'"><i class="glyphicon glyphicon-home"></i></a> <span class="breadcrum-sep">/</span></li>';

                 // Blog Category
        	    if ( is_category() ) {
        		    echo '<li><span class="active">' . __('Archive by category','wpt').' "' . single_cat_title('', false) . '"</span></li>';
                    
                    //echo "category";
                 }
        	    // Blog Day
        	    
                elseif ( is_day() ) {
        		    echo '<li><a href="'. get_year_link(get_the_time('Y')) . '">'. get_the_time('Y') .'</a> <span class="breadcrum-sep">/</span></li>';
        		    echo '<li><a href="'. get_month_link(get_the_time('Y'),get_the_time('m')) .'">'. get_the_time('F') .'</a> <span class="breadcrum-sep">/</span></li>';
        		    echo '<li><span class="active">'. get_the_time('d') .'</span></li>';
                    
                    //echo "blog day";
                } 
        	    // Blog Month
        	    
                elseif ( is_month() ) {
        		    echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> <span class="breadcrum-sep">/</span></li>';
        		    echo '<li><span class="active">'. get_the_time('F') .'</span></li>';
                    
                    //echo "month";
                }
        	    // Blog Year
        	     elseif ( is_year() ) {
        		    echo '<li><span class="active">'. get_the_time('Y') .'</span></li>';
                    
                    //echo "year";
                }
                
                elseif (  get_post_type() == 'post'  ) {
                	global $post;
                	
				      $post_type = get_post_type_object(get_post_type());
				      echo '<li><span class="active">'. $post_type->labels->singular_name.'</span></li>';
				      
                }
              	
              	
        	    // Single Post
        	     elseif ( is_single() && !is_attachment() ) {
        		
        		// Custom post type
        		if ( get_post_type() != 'post' ) {
        			$post_type = get_post_type_object(get_post_type());
        			$slug = $post_type->rewrite;
        		    
                    echo '<li><a href="' . get_permalink(get_page_by_title( 'projects' ) ) .'">' . $post_type->labels->singular_name . '</a> <span class="breadcrum-sep">/</span></li>';
        			
                    
        			if ($showCurrent == 1) echo '<li><span class="active">'. get_the_title() .'</span></li>';
        			//echo get_the_title(); 
        		//	echo '<li><a href="' . curPageURL() . '">'. wp_title( '',false ) .'</a></li>';
                    	
        		}
                // Blog post
                else {
        			$cat = get_the_category(); 
        			$cat = $cat[0];
                   
        			echo '<li>';
        				echo get_category_parents($cat, TRUE, ' <span class="breadcrum-sep">/</span>');
        			echo '</li>';
        			echo '<li><span class="active">'. wp_title( '',false ) .'</span></li>';
                    
                   // echo $cat;
        		}

        	    // Taxonomy
        	    } 
                elseif( get_post_taxonomies() ) {
        		
        		    $post_type = get_post_type_object(get_post_type());
        		    if( $post_type->name == 'portfolio' ) {
                   
                        echo '<li><a href="' . get_permalink(get_page_by_title( 'projects' ) ) . '">'. $post_type->name .'</a><span class="breadcrum-sep">/</span></li>';
                  
        		    }
                    
                    if(is_tax() ){
                        $queried_object = get_queried_object(); 
                        $term_id = $queried_object->term_id; 
                        $term_obj = get_term( $term_id, "portfolio_category"); 
                         
                        echo '<li><span class="active">'. $term_obj->name .'</span></li>';
                    
                    }
        		
        	    } 
                // Page with parent
                elseif ( is_page() && $post->post_parent ) {
                	
        		    $parent_id  = $post->post_parent;
        		    $breadcrumbs = array();
        		    while ($parent_id) {
            			$page = get_page($parent_id);
            			$breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a> <span class="breadcrum-sep">/</span></li>';
            			$parent_id  = $page->post_parent;
                        
        		    }
        		
                    $breadcrumbs = array_reverse($breadcrumbs);
                    for ($i = 0; $i < count($breadcrumbs); $i++) {
                        echo $breadcrumbs[$i];
                    }
                    
                  if ($showCurrent == 1) echo '<li><span class="active">'. get_the_title() .'</span></li>';
              
        	    } 
                // Default
                else {
                    
        		    echo '<li><span class="active">'. get_the_title() .'</span></li>';
        	    }
        echo '</ul>';
	echo '</span>';
}
   
  
   
    
   /* function the_breadcrumb() {
        
    	  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    	  $delimiter = '&raquo;'; // delimiter between crumbs
    	  $home = 'Home'; // text for the 'Home' link
    	  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    	  $before = '<span class="current">'; // tag before the current crumb
    	  $after = '</span>'; // tag after the current crumb
    	  
    	  global $post;
    	  $homeLink = get_bloginfo('url');
    	  //echo $homeLink; die;
    	  if (is_home() || is_front_page()) {
              if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
    	   } 
          
          else {
              echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
    	  
    	        if ( is_category() ) {
    	             $thisCat = get_category(get_query_var('cat'), false);
             
    	              if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
    	                  echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;
    	        }
                
                elseif ( is_search() ) {
    	            echo $before . 'Search results for "' . get_search_query() . '"' . $after;
    	        } 
            
                elseif ( is_day() ) {
    	             echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
    	             echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
    	             echo $before . get_the_time('d') . $after;
    	  
    	        } 
                
                elseif ( is_month() ) {
    	             echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
    	             echo $before . get_the_time('F') . $after;
    	  
    	        } 
                elseif ( is_year() ) {
    	             echo $before . get_the_time('Y') . $after;
    	  
    	        } 
                elseif ( is_single() && !is_attachment() ) {
    	            if ( get_post_type() != 'post' ) {
    	                $post_type = get_post_type_object(get_post_type());
    	                $slug = $post_type->rewrite;
    	                echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
    	                if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
    	            } 
                    else {
            	        $cat = get_the_category(); $cat = $cat[0];
            	        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
            	        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
            	        echo $cats;
            	        if ($showCurrent == 1) echo $before . get_the_title() . $after;
    	            }
    	  
    	        } 
                
                elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            	      $post_type = get_post_type_object(get_post_type());
            	      echo $before . $post_type->labels->singular_name . $after;
    	  
    	        } 
                elseif ( is_attachment() ) {
            	      $parent = get_post($post->post_parent);
            	      $cat = get_the_category($parent->ID); $cat = $cat[0];
            	      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
            	      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
            	      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
            	  
    	        } 
                elseif ( is_page() && !$post->post_parent ) {
        	          if ($showCurrent == 1) echo $before . get_the_title() . $after;
    	  
    	        } 
                elseif ( is_page() && $post->post_parent ) {
            	    $parent_id  = $post->post_parent;
            	    $breadcrumbs = array();
            	    while ($parent_id) {
                	    $page = get_page($parent_id);
                	    $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                	    $parent_id  = $page->post_parent;
            	    }
    	            $breadcrumbs = array_reverse($breadcrumbs);
            	    for ($i = 0; $i < count($breadcrumbs); $i++) {
            	        echo $breadcrumbs[$i];
            	        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
            	    }
    	            if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
    	  
    	        } 
                elseif ( is_tag() ) {
    	          echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
    	  
    	        } 
            elseif ( is_author() ) {
    	       global $author;
    	      $userdata = get_userdata($author);
    	      echo $before . 'Articles posted by ' . $userdata->display_name . $after;
    	  
    	    } elseif ( is_404() ) {
    	      echo $before . 'Error 404' . $after;
    	    }
    	  
    	    if ( get_query_var('paged') ) {
    	      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
    	      echo __('Page') . ' ' . get_query_var('paged');
    	      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    	    }
    	  
    	    echo '</div>';
    	  
    	  }
      
    
      
	}*/
/*
 * Replace Taxonomy slug with Post Type slug in url
 * Version: 1.1
 */
function wpt_taxonomy_slug_rewrite($wp_rewrite) {
    $rules = array();
    // get all custom taxonomies
    $taxonomies = get_taxonomies(array('_builtin' => false), 'objects');
    // get all custom post types
    $post_types = get_post_types(array('public' => true, '_builtin' => false), 'objects');
     
    foreach ($post_types as $post_type) {
        foreach ($taxonomies as $taxonomy) {
         
            // go through all post types which this taxonomy is assigned to
            foreach ($taxonomy->object_type as $object_type) {
                 
                // check if taxonomy is registered for this custom type
                if ($object_type == $post_type->rewrite['slug']) {
             
                    // get category objects
                    $terms = get_categories(array('type' => $object_type, 'taxonomy' => $taxonomy->name, 'hide_empty' => 0));
             
                    // make rules
                    foreach ($terms as $term) {
                        $rules[$object_type . '/' . $term->slug . '/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug;
                    }
                }
            }
        }
    }
    // merge with global rules
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
}
add_filter('generate_rewrite_rules', 'wpt_taxonomy_slug_rewrite');


/* ---------------------------------------------------------------------------
*	TGM Plugin Activation
* --------------------------------------------------------------------------- */

add_action( 'tgmpa_register', 'wpt_register_required_plugins' );
function wpt_register_required_plugins() {

	$plugins = array(
			array(
					'name'     				=> 'Responsive Contact Form', // The plugin name
					'slug'     				=> 'responsive-contact-form', // The plugin slug (typically the folder name)
					'source'   				=> THEME_DIR. '/functions/plugins/responsive-contact-form.zip', // The plugin source
					'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
					'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
					'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
					'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
					'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			
			array(
					'name'     				=> 'Responsive Contact Form Mailchimp Extension', // The plugin name
					'slug'     				=> 'responsive-contact-form-mailchimp-extension', // The plugin slug (typically the folder name)
					'source'   				=> THEME_DIR. '/functions/plugins/responsive-contact-form-mailchimp-extension.zip', // The plugin source
					'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
					'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
					'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
					'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
					'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			
			array(
					'name'     				=> 'Swift Custom Post Navigator', // The plugin name
					'slug'     				=> 'swift-custom-post-navigator', // The plugin slug (typically the folder name)
					'source'   				=> THEME_DIR. '/functions/plugins/swift-custom-post-navigator.zip', // The plugin source
					'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
					'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
					'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
					'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
					'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
	);

	// Change this to your theme text domain, used for internationalising strings
	$theme_text_domain = 'wpt';

	$config = array(
			'domain'       		=> $theme_text_domain,         	// Text domain - likely want to be the same as your theme.
			'default_path' 		=> '',                         	// Default absolute path to pre-packaged plugins
			'parent_menu_slug' 	=> 'themes.php', 				// Default parent menu slug
			'parent_url_slug' 	=> 'themes.php', 				// Default parent URL slug
			'menu'         		=> 'install-required-plugins', 	// Menu slug
			'has_notices'      	=> true,                       	// Show admin notices or not
			'is_automatic'    	=> true,					   	// Automatically activate plugins after installation or not
			'message' 			=> '',							// Message to output right before the plugins table
			'strings'      		=> array(
					'page_title'                       			=> __( 'Install Required Plugins', $theme_text_domain ),
					'menu_title'                       			=> __( 'Install Plugins', $theme_text_domain ),
					'installing'                       			=> __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
					'oops'                             			=> __( 'Something went wrong with the plugin API.', $theme_text_domain ),
					'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
					'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
					'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
					'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
					'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
					'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
					'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
					'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
					'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
					'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
					'return'                           			=> __( 'Return to Required Plugins Installer', $theme_text_domain ),
					'plugin_activated'                 			=> __( 'Plugin activated successfully.', $theme_text_domain ),
					'complete' 									=> __( 'All plugins installed and activated successfully. %s', $theme_text_domain ), // %1$s = dashboard link
					'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
			)
	);
	tgmpa( $plugins, $config );
}

?>