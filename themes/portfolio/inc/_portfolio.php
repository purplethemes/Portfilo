<?php
if ( ! class_exists( 'Portfolio_Post_Type' ) ) :

	class Portfolio_Post_Type {

		function __construct() 
        {

			// Runs when the plugin is activated
			register_activation_hook( __FILE__, array( &$this, 'plugin_activation' ) );

			// Add support for translations
			load_plugin_textdomain( 'wpt', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

			// Adds the portfolio post type and taxonomies
			add_action( 'init', array( &$this, 'portfolio_init' ) );
            
            // Adds meta boxes
            add_action( 'add_meta_boxes', array( &$this, 'portfolio_init_add_metaboxes' ) );
            
            //Save meta-box values
            add_action('save_post', array( &$this, 'save_portfolioposttype_values' ));
            
			// Thumbnail support for portfolio posts
			add_theme_support( 'post-thumbnails', array( 'portfolio' ) );

			// Adds thumbnails to column view
			add_filter( 'manage_edit-portfolio_columns', array( &$this, 'add_portfolio_thumbnail_column'), 10, 1 );
			add_action( 'manage_posts_custom_column', array( &$this, 'display_portfolio_thumbnail' ), 10, 1 );
            
           
			// Allows filtering of posts by taxonomy in the admin view
			add_action( 'restrict_manage_posts', array( &$this, 'add_taxonomy_filters' ) );
            
            //Allows filtering of post using portfolio meta field 
            add_filter( 'parse_query', array( &$this, 'portfolio_admin_posts_filter_data' ) );
            add_action( 'restrict_manage_posts', array( &$this, 'portfolio_admin_posts_filter_restrict_manage_posts' ) );
  
                        
			// Show portfolio post counts in the dashboard
			add_action( 'dashboard_glance_items', array( &$this, 'add_portfolio_counts' ) );

			// Give the portfolio menu item a unique icon
			add_action( 'admin_head', array( &$this, 'portfolio_icon' ) );
		}

		/**
		 * Flushes rewrite rules on plugin activation to ensure portfolio posts don't 404
		 * http://codex.wordpress.org/Function_Reference/flush_rewrite_rules
		 */

		function plugin_activation() 
        {
			$this->portfolio_init();
			flush_rewrite_rules();
		}

		function portfolio_init() 
        {

			/**
			 * Enable the Portfolio custom post type
			 * http://codex.wordpress.org/Function_Reference/register_post_type
			 */

			$labels = array(
				'name' => __( 'Portfolio', 'wpt' ),
				'singular_name' => __( 'Portfolio Item', 'wpt' ),
				'add_new' => __( 'Add New Item', 'wpt' ),
				'add_new_item' => __( 'Add New Portfolio Item', 'wpt' ),
				'edit_item' => __( 'Edit Portfolio Item', 'wpt' ),
				'new_item' => __( 'Add New Portfolio Item', 'wpt' ),
				'view_item' => __( 'View Item', 'wpt' ),
				'search_items' => __( 'Search Portfolio', 'wpt' ),
				'not_found' => __( 'No portfolio items found', 'wpt' ),
				'not_found_in_trash' => __( 'No portfolio items found in trash', 'wpt' )
			);

			$args = array(
				'labels' => $labels,
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true, 
        		'query_var' => true,
				'supports' => array( 'title', 'editor','thumbnail','page-attributes'),
				'capability_type' => 'post',
				'rewrite' => array("slug" => "portfolio"), // Permalinks format
                'menu_icon' => 'dashicons-portfolio',
				'menu_position' => 5,
				'has_archive' => true
			);

			$args = apply_filters('portfolioposttype_args', $args);

			register_post_type( 'portfolio', $args );
            flush_rewrite_rules();
            
			/**
			 * Register a taxonomy for Portfolio Tags
			 * http://codex.wordpress.org/Function_Reference/register_taxonomy
			 */

			$taxonomy_portfolio_tag_labels = array(
				'name' => __( 'Portfolio Tags', 'wpt' ),
				'singular_name' => __( 'Portfolio Tag', 'wpt' ),
				'search_items' => __( 'Search Portfolio Tags', 'wpt' ),
				'popular_items' => __( 'Popular Portfolio Tags', 'wpt' ),
				'all_items' => __( 'All Portfolio Tags', 'wpt' ),
				'parent_item' => __( 'Parent Portfolio Tag', 'wpt' ),
				'parent_item_colon' => __( 'Parent Portfolio Tag:', 'wpt' ),
				'edit_item' => __( 'Edit Portfolio Tag', 'wpt' ),
				'update_item' => __( 'Update Portfolio Tag', 'wpt' ),
				'add_new_item' => __( 'Add New Portfolio Tag', 'wpt' ),
				'new_item_name' => __( 'New Portfolio Tag Name', 'wpt' ),
				'separate_items_with_commas' => __( 'Separate portfolio tags with commas', 'wpt' ),
				'add_or_remove_items' => __( 'Add or remove portfolio tags', 'wpt' ),
				'choose_from_most_used' => __( 'Choose from the most used portfolio tags', 'wpt' ),
				'menu_name' => __( 'Portfolio Tags', 'wpt' )
			);

			$taxonomy_portfolio_tag_args = array(
				'labels' => $taxonomy_portfolio_tag_labels,
				'public' => true,
				'show_in_nav_menus' => true,
				'show_ui' => true,
				'show_tagcloud' => true,
				'hierarchical' => false,
				'rewrite' => array( 'slug' => 'portfolio_tag' ),
				'show_admin_column' => true,
				'query_var' => true
			);

			register_taxonomy( 'portfolio_tag', array( 'portfolio' ), $taxonomy_portfolio_tag_args );

		    /**
			 * Register a taxonomy for Portfolio Categories
			 * http://codex.wordpress.org/Function_Reference/register_taxonomy
			 */

			$taxonomy_portfolio_category_labels = array(
				'name' => __( 'Portfolio Categories', 'wpt' ),
				'singular_name' => __( 'Portfolio Category', 'wpt' ),
				'search_items' => __( 'Search Portfolio Categories', 'wpt' ),
				'popular_items' => __( 'Popular Portfolio Categories', 'wpt' ),
				'all_items' => __( 'All Portfolio Categories', 'wpt' ),
				'parent_item' => __( 'Parent Portfolio Category', 'wpt' ),
				'parent_item_colon' => __( 'Parent Portfolio Category:', 'wpt' ),
				'edit_item' => __( 'Edit Portfolio Category', 'wpt' ),
				'update_item' => __( 'Update Portfolio Category', 'wpt' ),
				'add_new_item' => __( 'Add New Portfolio Category', 'wpt' ),
				'new_item_name' => __( 'New Portfolio Category Name', 'wpt' ),
				'separate_items_with_commas' => __( 'Separate portfolio categories with commas', 'wpt' ),
				'add_or_remove_items' => __( 'Add or remove portfolio categories', 'wpt' ),
				'choose_from_most_used' => __( 'Choose from the most used portfolio categories', 'wpt' ),
				'menu_name' => __( 'Portfolio Categories', 'wpt' ),
			);

			$taxonomy_portfolio_category_args = array(
				'labels' => $taxonomy_portfolio_category_labels,
				'public' => true,
				'show_in_nav_menus' => true,
				'show_ui' => true,
				'show_admin_column' => true,
				'show_tagcloud' => true,
				'hierarchical' => true,
				'rewrite' => array( 'slug' => 'portfolio' ),
				'query_var' => true
			);

			register_taxonomy( 'portfolio_category', array( 'portfolio' ), $taxonomy_portfolio_category_args );

        }
            
        //Adding meta-box for Portfolio
           
        function portfolio_init_add_metaboxes()
        {
            add_meta_box("add_client_meta", "Portfolio Item Option", array( &$this, 'add_portfolioposttype_metaboxes' ), "portfolio", "normal", "low");
                    
            add_meta_box("add_image_meta", "Portfolio Image Item Option", array( &$this, 'add_portfolio_imageposttype_metaboxes' ), "portfolio", "normal", "low");
                    
            wp_enqueue_style( 'portfoliotheme-post-option', get_template_directory_uri() . '/css/portfolio-post-option.css', array( 'portfoliotheme-post-option', 'genericons' ), '20131205' );
                    
            wp_enqueue_script( 'portfoliotheme-portfolio-post', get_template_directory_uri() . '/js/portfolio-post.js', array( 'jquery' ), '20140616', true );
        
        }
                     
        function add_portfolioposttype_metaboxes()
        {
            global $post;
            $custom = get_post_custom($post->ID);
            $client = $custom["client"][0];
            $url = $custom["url"][0];
            $url_text = $custom["url_text"][0];
            ?>
            <label><?php _e('Client:', 'wpt');?></label>
            <input name="client" value="<?php echo esc_attr($client);?>" />
                      
            <br/>
            <em><?php _e('Project description: Client.', 'wpt'); ?></em>
            <br/>
            <br/>
            
            <label><?php _e('Url Text:', 'wpt');?></label>
            <input name="url_text" value="<?php echo esc_attr($url_text);?>" />
            <br/>
            <em><?php _e('Project description: URL', 'wpt'); ?></em>
            <br/>
            <br/>
            
            <label>URL:</label>
            <input name="url" value="<?php echo esc_url($url);?>" />
            <br/>
            <em><?php _e('Project description: Eg:http://www.websitename.com.', 'wpt'); ?></em>
            
            
            
            <?php 
        } 
                    
                
        // Adding meta-box for image post type 
                 
        function  add_portfolio_imageposttype_metaboxes() {
                        
                        global $post;
                        $values = get_post_custom( $post->ID );  
			            $imagebox = isset( $values['image_id'] ) ? esc_attr( $values['image_id'][0] ) : "1";
                        ?>
                        <p>
            				<h3 class="hndle">
            				<a href="javascript:void(0);" class="add button button-primary button-sm" style="float:right;">+ Add More</a>
            				<span><?php _e('Manage Portfolio Slider Images', 'wpt'); ?></span></h3>
            			</p>
            			
            			<div class="portfolio_meta_control">
            				<div class="imageDetailsClone">
            				<?php for($i=0;$i<$imagebox;$i++):
            						
            						$portfolio_upload_attach_id = isset( $values['imagebox'.$i] ) ? esc_attr( $values['imagebox'.$i][0] ) : "";   
            						$portfolio_upload_image_src = wp_get_attachment_image_src($portfolio_upload_attach_id, 'thumbnail');
            						
                                    
            						$portfolioCheckImg = "none";
            						if(!empty($portfolio_upload_image_src[0]))
            							$portfolioCheckImg = "inline-block";
            				?>
            					<div class="postbox clone imgbox-<?php echo $i;?>">
            						<div class="handlediv" title="Click to toggle"><br></div>
            						<h3 class="hndle"><span><?php _e('Portfolio Image Details', 'wpt'); ?></span></h3>
            						<div class="inside" style="margin-left: 20px;">
            							<div class="form-field">
            								<label for="cover_image"><?php _e('Portfolio Image', 'wpt'); ?></label>
            								<div class="cover_image" style="display:<?php echo esc_attr($portfolioCheckImg)?>;">
            								  <img src="<?php echo esc_attr($portfolio_upload_image_src[0]); ?>" name="slider_display_cover_image" />
            								</div>
            								<p><span><i><?php _e('Best image size :', 'wpt'); ?> <strong><?php _e('1600px * 837px', 'wpt'); ?></strong> <?php _e('(upload : JPG, PNG & GIF )', 'wpt');?></i></span></p>
            								
            								<input type="hidden" size="36" name="slider_upload_image[]" value="<?php echo esc_attr($portfolio_upload_attach_id); ?>" />
            								<p>
            									<input name="slider_upload_image_button" type="button" value="Upload" class="portfolio_image_issue button button-primary"/>
            									<input name="slider_remove_image_button" type="button" value="Remove Image"  width="8%" class="portfolio_remove_issue button button-primary" style="display:<?php echo esc_attr($portfolioCheckImg);?>;">
            								</p>
            							</div>
            						</div>
            						<?php if($i > 0):?>
            						<div class="hr" style="margin-bottom: 10px;"></div>
            						<p style="overflow:hidden; padding-right:10px;">
            							<a href="javascript:void(0);" onclick="removebox('<?php echo $i;?>');" class="btn-right button button-remove button-sm"><?php _e('- Remove', 'wpt'); ?></a>
            						</p>
            						<?php endif;?>
            					</div>
            				<?php endfor;?>
            				</div>
            			</div> <!-- #end main div -->
            			<input type="hidden" name="image_id" value="<?php echo esc_attr($imagebox);?>">
                        
                      <?php 
        } 
                    
                   
                   
        // Function to Save meta-box values
            
        function save_portfolioposttype_values( $post_id ){
                 global $post;
                 update_post_meta($post->ID, "client", $_POST["client"]);
                 update_post_meta($post->ID, "url_text", $_POST["url_text"]);
                 update_post_meta($post->ID, "url", $_POST["url"]);
                         
                          // for image box    
            			    if( isset( $_POST['image_id'] ) )  
            				update_post_meta( $post_id, 'image_id', $_POST['image_id']);
                         
                             for($i=0;$i<$_POST['image_id'];$i++){
                            // for image
            				if( isset( $_POST['slider_upload_image'][$i] ) )  
            				update_post_meta( $post_id, 'imagebox'.$i , $_POST['slider_upload_image'][$i]);
            				}
                        }           

		/**
		 * Add Columns to Portfolio Edit Screen
		 * http://wptheming.com/2010/07/column-edit-pages/
		 */
        
        
		function add_portfolio_thumbnail_column( $columns ) {
            unset($columns['taxonomy-portfolio_tag']);

			$column_thumbnail = array( 'thumbnail' => __('Thumbnail','wpt' ) );
            
            $column_client = array( 'client' => __('Client','wpt' ) );
            
			$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
            $columns = array_slice( $columns, 0, 3, true ) + $column_client + array_slice( $columns, 3, NULL, true );
          
			return $columns;
		}

		function display_portfolio_thumbnail( $column ) {
			global $post;
			switch ( $column ) {
				case 'thumbnail':
					echo get_the_post_thumbnail( $post->ID, 'cpt-logo-thumbnail' );
					break;
                    
                case 'client':
					echo get_post_meta($post->ID, 'client', true );
					break;
                
			}
		}
        
       
		/**
		 * Adds taxonomy filters to the portfolio admin page
		 * 
		 */

		function add_taxonomy_filters() {
			global $typenow;
            
			// An array of all the taxonomies you want to display. Use the taxonomy name or slug
			$taxonomies = array( 'portfolio_category');

			// must set this to the post type you want the filter(s) displayed on
			if ( $typenow == 'portfolio' ) {

				foreach ( $taxonomies as $tax_slug ) {
					$current_tax_slug = isset( $_GET[$tax_slug] ) ? $_GET[$tax_slug] : false;
					$tax_obj = get_taxonomy( $tax_slug );
                    
					$tax_name = $tax_obj->labels->name;
                    
					$terms = get_terms($tax_slug);
					if ( count( $terms ) > 0) {
						echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
						echo "<option value=''>$tax_name</option>";
						foreach ( $terms as $term ) {
                           
							echo '<option value=' . $term->slug, $current_tax_slug == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
						}
						echo "</select>";
					}
				}
			}
		}
           // Add custom field filter on admin page
           
        function portfolio_admin_posts_filter_data( $query) {
               global $pagenow;
                    
	                    if ( is_admin() && $pagenow=='edit.php' && isset($_GET['portfolio_client_filter_articles']) && !empty($_GET['portfolio_client_filter_articles'])) {
	                        
	                        $query->query_vars['meta_value'] = $_GET['portfolio_client_filter_articles'] ;
	                   
	                    }
                    }
                   
                    
        function portfolio_admin_posts_filter_restrict_manage_posts() {
                    
              global $wpdb, $typenow,$wp_query;
					
					$clients=$wpdb->get_col("
						SELECT meta.meta_value FROM ".$wpdb->postmeta." as meta 
						LEFT JOIN ".$wpdb->posts." AS posts ON (posts.ID=meta.post_id) 
						WHERE meta.meta_key = 'client' 
						AND posts.post_type = 'portfolio'
						ORDER BY meta.meta_value 
					");				
                    if ($typenow=='portfolio'){
                    ?>
					    <select name="portfolio_client_filter_articles" id="client">
					        <option value=""><?php _e( 'Show all client', 'wpt'  ); ?></option>
					        <?php foreach ($clients as $client) { ?>
					        <option value="<?php echo esc_attr( $client ); ?>" <?php if(isset($_GET['portfolio_client_filter_articles']) && !empty($_GET['portfolio_client_filter_articles']) ) selected($_GET['portfolio_client_filter_articles'], $client); ?>>
							<?php echo esc_attr( $client ); ?>
					        </option>
					        <?php } ?>
					    </select>
					    <?php
                    }
                    } 
              
		/**
		 * Add Portfolio count to "Right Now" Dashboard Widget
		 */

		function add_portfolio_counts() {
			if ( ! post_type_exists( 'portfolio' ) ) {
				return;
			}

			$num_posts = wp_count_posts( 'portfolio' );
			$num = number_format_i18n( $num_posts->publish );
			$text = _n( 'Portfolio Item', 'Portfolio Items', intval($num_posts->publish) );
			if ( current_user_can( 'edit_posts' ) ) {
				$output = "<a href='edit.php?post_type=portfolio'>$num $text</a>";
			}
			echo '<li class="post-count portfolio-count">' . $output . '</li>';

			if ($num_posts->pending > 0) {
				$num = number_format_i18n( $num_posts->pending );
				$text = _n( 'Portfolio Item Pending', 'Portfolio Items Pending', intval($num_posts->pending) );
				if ( current_user_can( 'edit_posts' ) ) {
					$num = "<a href='edit.php?post_status=pending&post_type=portfolio'>$num</a>";
				}
				echo '<li class="post-count portfolio-count">' . $output . '</li>';
			}
		}

		/**
		 * Displays the custom post type icon in the dashboard
		 */

		function portfolio_icon() { ?>
        <style type="text/css" media="screen">
           .portfolio-count a:before{content:"\f322"!important}
        </style>
		<?php }

	}

	new Portfolio_Post_Type;

endif;

?>