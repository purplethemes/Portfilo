<?php
/**
 * The template for displaying all pages
 *
 * Portfoliotheme functions and definitions
 * @package Portfolio
 * @author Purplethemes
 */
get_header(); 

global $wp_query, $portfolio_options;

$post_id = $wp_query->get_queried_object_id();
/*$content_post = get_post($post_id);
$content = $content_post->post_content;
$current_title = get_the_title($post_id);*/


$lt = get_post_meta($post_id, 'Layout', true);


switch ( get_post_meta($post_id, 'Layout', true) ) {
	
	case 'left_sidebar':
		$class = 'left';
        break;
	case 'right_sidebar':
		$class = 'right';
		break;
	case 'both_sidebar':
		$class = 'both';
		break;
	default:
		$class = 'full';
		break;
}
 
if($class == 'left'){
  
    $right_class = 'col-xs-12 col-sm-9 col-md-9 pull-right';
    $left_class = 'col-xs-12 col-sm-3 col-md-3 pull-left';
    $class = 'left';
}
     
elseif($class == 'right'){
    
    $right_class = 'col-xs-12 col-sm-9 col-md-9';
    $left_class = 'col-xs-12 col-sm-3 col-md-3';
    $class = 'right';
}

elseif($class == 'both'){
    
    $center_class = 'col-xs-12 col-sm-6 col-md-6';
    $left_class = 'col-xs-12 col-sm-3 col-md-3';
    $class = 'both';
    
} 

else{
	
	$right_class = '';
	$class = 'full';
}


?>
<section class="container">
        <?php
		if ( is_front_page() ) {
		
	        get_template_part( 'libs/content', 'homepage' );
	    	
		}
		else
		{
		
	    ?>
	        <!-- about section -->
	        <section class="about-section">
	        
				<?php if($class)
				{
				
					if($class == 'both')
					{
						echo '<article class="' .$left_class. '">';
		                                echo'<aside>';
		                                	echo'<div class="sidebar">';
		                                            get_sidebar();
		                                    echo '</div>';
		                                echo '</aside>';
		                    echo'</article>';
		                    
		                    echo'<article class="' .$center_class.'">';
										echo '<section class="row">';
		                    				echo '<div class="about-overview">';
		                    							get_template_part( 'libs/content', 'page' );
		                    				echo '</div>';
		                    			echo '</section>';
								echo '</article>';
		                
					} 
					
					else
					{
						echo'<article class="' .$right_class.'">';
										echo '<section class="row">';
			                    			echo '<div class="about-overview">';
			                    						get_template_part( 'libs/content', 'page' );
			                    			echo '</div>';
			                    		echo '</section>';
								echo '</article>';
					}
				
				}				
				
				if($class){ 
				
						if($class == 'full')
						{
							echo '';
						}
						else{
							
							echo '<article class="' .$left_class. '">';
		                            echo'<aside>';
		                                echo'<div class="sidebar">';
		                                	//include( TEMPLATEPATH . '/sidebar.php');
		                                    include( TEMPLATEPATH . '/sidebar-blog.php');
		                                echo '</div>';
		                            echo '</aside>';
		                    echo'</article>';
						}
		               
	                } 
	 
	?>			
			
	        <!-- sidebar section end -->
	        </section>
	        <!-- about section end -->
<?php } 
//echo do_shortcode( '[testimonial count="4"]' );

?>


</section>
<?php
 echo do_shortcode( '[counter]' ); 

 get_footer(); ?>
