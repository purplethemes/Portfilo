<?php
######
## Verification Image
######
## This class generate an image with random text
## to be used in form verification. It has visual
## elements design to confuse OCR software preventing
## the use of BOTS.
class ImageVerification{
	

	function ImageVerification()
	{
		if(!isset($_SESSION)){
    session_start();
}
		
		$this->fonts = array(	'libs/fonts/arial.ttf',		'libs/fonts/ariblk.ttf',		'libs/fonts/ariali.ttf',
								'libs/fonts/times.ttf',		'libs/fonts/timesi.ttf',		'libs/fonts/timesbd.ttf',
								'libs/fonts/verdana.ttf',	'libs/fonts/verdanai.ttf',		'libs/fonts/verdanab.ttf',
								'libs/fonts/ZENOE___.TTF'); 
		
		$this->GDfonts = array(	'libs/fonts/8x13iso.gdf',	'libs/fonts/aha.gdf',			'libs/fonts/andale12.gdf',
								'libs/fonts/Bastarda.gdf',	'libs/fonts/courier8.gdf',		'libs/fonts/proggyclean.gdf',
								'libs/fonts/trisk.gdf',		'libs/fonts/reize.gdf',			'libs/fonts/terminal6.gdf',
								'libs/fonts/proggysquare.gdf'); 
		//$this->GDfonts = array(	'libs/fonts/ZenonExtended.gdf'); 

		$this->img_width = 150; #largura da imagem
		$this->img_height = 35; #altura da imagem
		
		$this->minAngle = -30; #ngulo mnimo de rotao dos caracteres
		$this->maxAngle = 30; #ngulo mximo de rotao dos caracteres
		
		$this->minSize = 13; #tamanho mnimo da fonte dos caracteres
		$this->maxSize = 22; #tamanho mximo da fonte dos caracteres
		
		$this->strLength = 4;
		$this->caseSensitive = false; # comparao case-sensitive
		
		// calculate spacing between characters based on width of image
		$this->iSpacing = (int)($this->img_width / $this->strLength);

		$this->img_TypeArr = array('PNG','JPG','GIF'); 
		$this->img_Type = '0'; 

		$this->img_BgSet = true; 
		$this->img_Bg = array(	'libs/imageBg/1.png',	'libs/imageBg/2.png',	'libs/imageBg/3.png',
								'libs/imageBg/4.png',	'libs/imageBg/5.png',	'libs/imageBg/6.png',
								'libs/imageBg/7.png'); 
//		$this->img_Bg = array(	'libs/imageBg/4.png'); 
		$this->fill_Color = 1; 

		$this->round_disp = false; 
		$this->iNumRound = 2;

		$this->box_disp = false; 
		$this->iNumBox = 2;

		$this->line_disp = true; 
		$this->iNumLines = 0;

		$this->shap_disp = false; 

		$this->border_disp = false; 

		$this->freetype_font = true; 
		$this->gd_font_disp = false; 
	}
	
	function geraString()
	{
		$string = array();
		
		for($i=1;$i<=$this->strLength;$i++)
		{
			$numAleat = rand(1,3);
		
			switch($numAleat)
			{
				case '1':
					//letra maiuscula
					$min = 65;
					$max = 90;
					break;
					
				case '2':
					//letra minuscula
					$min = 97;
					$max = 122;
					break;
					
				case '3':
					//numeros
					$min = 48;
					$max = 57;
					break;
			}
			$string[] = chr(rand($min,$max));
		}
		shuffle($string);
		$_SESSION['stringVerificacao'] = implode('',$string);
	}
	
	function geraImagem()
	{
		$this->geraString();
		$this->string = $_SESSION['stringVerificacao'];
		
		if ($this->img_Type=='0')
			header("Content-type: image/png");
		elseif ($this->img_Type=='1')
			header("Content-type: image/jpeg");
		elseif ($this->img_Type=='2')
			header('Content-type: image/gif');
			
		if ($this->img_BgSet)
		{
			$bg = rand(0,sizeof($this->img_Bg)-1);

			if ($this->img_Type=='0')
				$this->img = imagecreatefrompng($this->img_Bg[$bg]);
			elseif ($this->img_Type=='1')
				$this->img = imagecreatefromjpeg($this->img_Bg[$bg]);
			elseif ($this->img_Type=='2')
				$this->img = imagecreatefromgif($this->img_Bg[$bg]);
		}
		else
		{
			// create new image
			//$this->img = imagecreatetruecolor($this->img_width,$this->img_height);
			$this->img = imagecreate($this->img_width,$this->img_height);
		}

		$this->arr_color = array(
							0 =>  	imagecolorallocate($this->img, 235, 235, 235),			//grey
							1 =>	imagecolorallocate($this->img, 255, 255, 255),			//white
							2 =>	imagecolorallocate($this->img, 0, 0, 0),				//black
							3 =>	imagecolorallocatealpha($this->img, 255, 0, 0, 75),		//red
							4 =>	imagecolorallocatealpha($this->img, 0, 255, 0, 75),		//green
							5 =>	imagecolorallocatealpha($this->img, 0, 0, 255, 75),		//blue
							6 =>	imagecolorallocate($this->img, 183, 183, 183),			//newgrey
							7 =>	imagecolorallocate($this->img, 111, 111, 111),			//othergrey
							8 =>	imagecolorallocate($this->img, 172, 172, 172),			//lightgrey
							);

		//Fill Color in Box
		if (!$this->img_BgSet)
			imagefilledrectangle($this->img, 0, 0, $this->img_width, $this->img_height, $this->arr_color[$this->fill_Color]);
		
		//Make Shape in Box
		if ($this->shap_disp)
		{
			$t = 4 / 2 - 0.5;
			$k = (rand(26,50) - rand(1,$this->img_height)) / (rand(101,$this->img_width) - rand(1,$this->img_width)); //y = kx + q
			$a = $t / sqrt(1 + pow($k, 2));
			$points = array(
							round(rand(1,$this->img_width) - (1+$k)*$a), round(rand(1,$this->img_height) + (1-$k)*$a),
							round(rand(1,$this->img_width) - (1-$k)*$a), round(rand(1,$this->img_height) - (1+$k)*$a),
							round(rand(101,$this->img_width) + (1+$k)*$a), round(rand(26,50) - (1-$k)*$a),
							round(rand(101,$this->img_width) + (1-$k)*$a), round(rand(26,50) + (1+$k)*$a),
							);    
		
			$iRandColour = rand(0, 10);
			$iShapColour = imagecolorallocate($this->img, $iRandColour, $iRandColour, $iRandColour);
			imagefilledpolygon($this->img, $points, 4, $iShapColour);
		}

		//each color 1 round display
		if ($this->round_disp)
		{
			for($i=0;$i<$this->iNumRound;$i++)
			{
				$iRandColour = rand(0, 10);
				$iRoundColour = imagecolorallocate($this->img, $iRandColour, $iRandColour, $iRandColour);
				imagefilledellipse($this->img, ceil(rand(10,$this->img_width)), ceil(rand(0,35)), rand(10,30), rand(10,30), $iRoundColour);
			}
		}

		//each color 1 rectangle display
		if ($this->box_disp)
		{
			for($i=0;$i<$this->iNumBox;$i++)
			{
				$iRandColour = rand(0, 10);
				$iBoxColour = imagecolorallocate($this->img, $iRandColour, $iRandColour, $iRandColour);
				imagefilledrectangle($this->img, rand(5,40), rand(5,40), rand(5,40), rand(5,40), $iBoxColour);
			}
		}

		//each color 5 line display
		if ($this->line_disp)
		{
			for($i=0;$i<$this->iNumLines;$i++)
			{
				$iRandColour = rand(190, 250);
				$iLineColour = imagecolorallocate($this->img, $iRandColour, $iRandColour, $iRandColour);
				imageline($this->img,rand(0,$this->img_width),rand(10,$this->img_height),rand(0, $this->img_width),rand(20,$this->iHeight), $iLineColour);
				imageline($this->img,rand(1,$this->img_width),rand(1,$this->img_height),rand(50,$this->img_width),rand(26,$this->iHeight),$iLineColour);
				imageline($this->img,rand(1,$this->img_width),rand(1,$this->img_height),rand(10,$this->img_width),rand(5,$this->iHeight),$iLineColour);
			}
		}
		
		//Image Border
		if ($this->border_disp)
		{
			$iRandColour = rand(0, 238);
			$iBorderColour = imagecolorallocate($this->img, $iRandColour, $iRandColour, $iRandColour);

			imagefilledrectangle($this->img, 0, 0, $this->img_width, 1, $iBorderColour);
			imagefilledrectangle($this->img, $this->img_width - 1, 0, $this->img_width - 1, $this->img_height - 1, $iBorderColour);
			imagefilledrectangle($this->img, 0, 0, 0, $this->img_height - 1, $iBorderColour);
			imagefilledrectangle($this->img, 0, $this->img_height - 1, $this->img_width, $this->img_height - 1, $iBorderColour);
		}

		// loop through and write out selected number of characters
		for($i=0;$i<strlen($this->string);$i++)
		{
			$size = rand($this->minSize,$this->maxSize);
			$angle = rand($this->minAngle,$this->maxAngle); // character show in one line or up down
			
			// select random greyscale colour
			//$iRandColour = rand(0, 128);
			//$iTextColour = imagecolorallocate($this->img, $iRandColour, $iRandColour, $iRandColour);
			//$iTextColour = imagecolorallocate($this->img, 0, 0, 0);			
			$iRandColour=1;			
			// select random font
			if ($this->freetype_font)
				$fonte = $this->fonts[rand(0,sizeof($this->fonts)-1)];
			elseif ($this->gd_font_disp)
				$fonte = imageloadfont($this->GDfonts[rand(0,sizeof($this->GDfonts)-1)]);
			else
				$fonte = rand(5, 10);

			// write text to image
			if ($this->freetype_font)
				imagettftext($this->img, $size, $angle, $this->iSpacing / 3 + $i * $this->iSpacing, ($this->img_height + 20) / 2, $iRandColour, $fonte, $this->string{$i});
			else
 				imagestring($this->img, $fonte, $this->iSpacing / 3 + $i * $this->iSpacing, rand(0,$this->img_height - imagefontheight($fonte)), $this->string{$i}, $iTextColour);
				
			//imagestring($this->img, $fonte, $this->iSpacing / 3 + $i * $this->iSpacing, ($this->img_height - imagefontheight($fonte)) / 2, $this->string{$i}, $iTextColour);
		}
		
		if ($this->img_Type=='0')
			imagepng($this->img);
		elseif ($this->img_Type=='1')
			imagejpeg($this->img);
		elseif ($this->img_Type=='2')
			imagegif($this->img);
	}

#verify code
	function leCodigos()
	{
		$this->codDig = $_POST['code_of_image'];
		$this->codSes = $_SESSION['stringVerificacao'];
		$this->codDig;
		$this->codSes;
	}
	
	function comparacaoCaseOn()
	{
		if ( strcmp($this->codDig,$this->codSes)==0 )
			return true;
		else
			return false;
	}
	
	function comparacaoCaseOff()
	{
		if ( strcasecmp($this->codDig,$this->codSes)==0 )
			return true;
		else
			return false;
	}
	
	function comparaCodigos()
	{
		if($this->caseSensitive)
			return $this->comparacaoCaseOn();
		else
			return $this->comparacaoCaseOff();
	}
}
?>