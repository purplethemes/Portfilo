<?php

function strip_html_tags( $text )
{
	// PHP's strip_tags() function will remove tags, but it
	// doesn't remove scripts, styles, and other unwanted
	// invisible text between tags.  Also, as a prelude to
	// tokenizing the text, we need to insure that when
	// block-level tags (such as <p> or <div>) are removed,
	// neighboring words aren't joined.
	$text = preg_replace(
		array(
			// Remove invisible content
			'@<[\s]head[\s\S\i][^>]*?>.([a-zA-Z0-9<i></i>s.-]*).?</head>@siu',
			'@<[\s]style[\s\S\i][^>]*?>.([a-zA-Z0-9<i></i>s.-]*).?</style>@siu',
			'@<[\s\S]script[\s\S\i][^>]*?.([a-zA-Z0-9.-[/s/S]]*).?</script>@siu',
			'@<[\s]object[\s\S\i][^>]*?.([a-zA-Z0-9<i></i>s.-]*).?</object>@siu',
			'@<[\s]embed[\s\S\i][^>]*?.([a-zA-Z0-9<i></i>s.-]*).?</embed>@siu',
			'@<[\s]applet[\s\S\i][^>]*?.([a-zA-Z0-9<i></i>s.-]*).?</applet>@siu',
			'@<[\s]noframes[\s\S\i][^>]*?.([a-zA-Z0-9<i></i>s.-]*).?</noframes>@siu',
			'@<[\s]noscript[\s\S\i][^>]*?.([a-zA-Z0-9<i></i>s.-]*).?</noscript>@siu',
			'@<[\s]noembed[\s\S\i][^>]*?.([a-zA-Z0-9<i></i>s.-]*).?</noembed>@siu',

			// Add line breaks before & after blocks
			'@<((br)|(hr))@iu',
			'@</?((address)|(blockquote)|(center)|(del))@iu',
			'@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
			'@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
			'@</?((table)|(th)|(td)|(caption))@iu',
			'@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
			'@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
			'@</?((frameset)|(frame)|(iframe))@iu',
		),
		array(
			' ', ' ',' ', ' ', ' ', ' ', ' ', ' ', ' ',
			"\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
			"\n\$0", "\n\$0",
		),
		$text );
		
	// Remove all remaining tags and comments and return.
	return strip_tags( $text );
}
?>