<?php
session_start();

include_once('libs/function.php');
include_once("libs/htmlMimeMail5/htmlMimeMail5.php");
$mail = new htmlMimeMail5();
/*
include_once('libs/ImgVerification.php');
$vImg = new ImageVerification();
$vImg->leCodigos();
$verifyCodeFlag = $vImg->comparaCodigos();*/
if($_REQUEST['Action'] == 'ContactUs')
{
	if( !empty($_REQUEST['code_of_image']))
	{	
		$_REQUEST['Name']=strip_tags($_REQUEST['Name']);
		$_REQUEST['Email']=strip_tags($_REQUEST['Email']);
		$_REQUEST['Message']=strip_tags($_REQUEST['Message']);
        $timestamp = date('d-m-Y H:i:s');
        $mail->setFrom(stripslashes($_REQUEST['Name']).' <'.$_REQUEST['Email'].'>');
		$mail->setSubject("Contact Request: Realestate"." Time :".$timestamp);
        
        $filename = "email_contact.html";
		$handle = fopen($filename, "r");
		$contents = fread($handle, filesize($filename));
		$contents = str_replace("{Name}", stripslashes($_REQUEST['Name']), $contents) ;
		$contents = str_replace("{Email}", $_REQUEST['Email'], $contents);
		$contents = str_replace("{Message}", stripslashes($_REQUEST['Message']), $contents) ;
		
		$mail->setHtml($contents);
         
				
		$result=$mail->send(array('application.qa@gmail.com'));			//for live
		
		fclose($handle);
        
        if($result)
		{
            $arr['MSG'] = "SUCCESS";
        }
    } 
    
    
   else
        {
           $arr['err'] = "invalidcode";
        }
         echo json_encode($arr) ;
 }

elseif($_REQUEST['Action'] == 'Inquiry')
{
	
		$_REQUEST['Name_contact']=strip_tags($_REQUEST['Name_contact']);
		$_REQUEST['EmailId']=strip_tags($_REQUEST['EmailId']);
        $_REQUEST['contact_no']=strip_tags($_REQUEST['contact_no']);
        $_REQUEST['subject']=strip_tags($_REQUEST['subject']);
		$_REQUEST['msg']=strip_tags($_REQUEST['msg']);
        $timestamp = date('d-m-Y H:i:s');
        $mail->setFrom(stripslashes($_REQUEST['Name_contact']).' <'.$_REQUEST['EmailId'].'>');
		$mail->setSubject("Contact Request: Realestate"." Time :".$timestamp);
        
        $filename = "email_inquiry.html";
		$handle = fopen($filename, "r");
		$contents = fread($handle, filesize($filename));
		$contents = str_replace("{Name_contact}", stripslashes($_REQUEST['Name_contact']), $contents) ;
		$contents = str_replace("{EmailId}", $_REQUEST['EmailId'], $contents);
        $contents = str_replace("{contact_no}", $_REQUEST['contact_no'], $contents);
        $contents = str_replace("{subject}", $_REQUEST['subject'], $contents);
		$contents = str_replace("{msg}", stripslashes($_REQUEST['msg']), $contents) ;
		
		$mail->setHtml($contents);
       
				
		$result=$mail->send(array('application.qa@gmail.com'));			//for live
		
		fclose($handle);
        
        if($result)
		{
            $arr['MSG'] = "SUCCESS";
        }
          echo json_encode($arr) ;
 }

elseif($_REQUEST['Action'] == 'Newsletter')
{
		//$_REQUEST['emailnewsletter']=strip_tags($_REQUEST['emailnewsletter']);
		$_REQUEST['emailnewsletter']=isset($_POST['value']) ? $_POST['value'] : '';
        $timestamp = date('d-m-Y H:i:s');
        $mail->setFrom(stripslashes($_REQUEST['emailnewsletter']));
		$mail->setSubject("Contact Request: Realestate"." Time :".$timestamp);
        
        $filename = "email_newsletter.html";
		$handle = fopen($filename, "r");
		$contents = fread($handle, filesize($filename));
		$contents = str_replace("{emailnewsletter}", $_REQUEST['emailnewsletter'], $contents);
       
		$mail->setHtml($contents);
         		
		$result=$mail->send(array('application.qa@gmail.com'));			//for live
		
		fclose($handle);
        
        if($result)
		{
            $arr['MSG'] = "SUCCESS";
        }
    
  
         echo json_encode($arr) ;
}


?>