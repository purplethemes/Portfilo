<?php
/*
* Customizer file
*
*
*/

function wpt_customize_register( $wp_customize ) {
   //All our sections, settings, and controls will be added here
   $wp_customize->remove_section('title_tagline');
  
  

/**----------------------------------------------------------------
* 
* Colors Section
* 
-------------------------------------------------------------------*/
  
$colors = array();

$colors[] = array(
  'slug'=>'bodybg_color', 
  'default' => '#ffffff',
  'label' => __('Body Background Color', 'wpt'),
  
);

$colors[] = array(
  'slug'=>'headerbg_color', 
  'default' => '#030501',
  'label' => __('Header Background Color', 'wpt')
);

$colors[] = array(
  'slug'=>'footerbg_color', 
  'default' => '#000000',
  'label' => __('Footer Background Color', 'wpt')
);

$colors[] = array(
  'slug'=>'menubg_color', 
  'default' => '#000000',
  'label' => __('Menu Background Color', 'wpt')
);

$colors[] = array(
  'slug'=>'menu_color', 
  'default' => '#ffffff',
  'label' => __('Menu Color', 'wpt')
);

$colors[] = array(
  'slug'=>'active_menu_color', 
  'default' => '#D9534F',
  'label' => __('Active Menu Color', 'wpt')
);

$colors[] = array(
  'slug'=>'hover_menu_color', 
  'default' => '#D9534F',
  'label' => __('Hover Menu Color', 'wpt')
);

$colors[] = array(
  'slug'=>'breadcrumb_bg_color', 
  'default' => '#f0f0f0',
  'label' => __('Breadcrumb Background Color', 'wpt')
);

$colors[] = array(
  'slug'=>'link_color', 
  'default' => '#8f8f8f',
  'label' => __('Link Color', 'wpt')
);

$colors[] = array(
  'slug'=>'link_hover_color', 
  'default' => '#D9534F',
  'label' => __('Link Hover Color', 'wpt')
);

$colors[] = array(
  'slug'=>'link_bg_color', 
  'default' => '#D9534F',
  'label' => __('Link Background Color', 'wpt')
);

$wp_customize->add_section( 'colors', array(
	'title' => __( 'Colors', 'wpt' ),
	'priority' => 3,
) );

foreach( $colors as $color ) {
  // SETTINGS
  $wp_customize->add_setting(
    $color['slug'], array(
      'default' => $color['default'],
      'sanitize_callback' => 'sanitize_hex_color',
      'type' => 'option', 
      'capability' => 'edit_theme_options'
    )
  );
  // CONTROLS
  $wp_customize->add_control(
    new WP_Customize_Color_Control(
      $wp_customize,
      $color['slug'], 
      array('label' => $color['label'], 
      'section' => 'colors',
      'settings' => $color['slug'])
    )
  );
}

/**----------------------------------------------------------------
* 
* Typography Section
* 
-------------------------------------------------------------------*/

$wp_customize->add_section( 'font' , array(
    'title'      => __('Typography', 'wpt'),
    'priority'   => 4,
));

// Heading Fonts
$wp_customize->add_setting( 'font_heading', array(
	        'default' => 'Open Sans'
	       
	    ) );
	    
 $wp_customize->add_control(
    'font_heading',
    array(
        'type' => 'select',
        'label' => 'Heading Font',
        'section' => 'font',
        'choices' => array(
            'Times New Roman' => 'Times New Roman',
            'Arial'     => 'Arial',
            'Courier New'   => 'Courier New',
			'Open Sans' => 'Open Sans',
			'Slabo'		=> 'Slabo',
			'Roboto'	=> 'Roboto',
			'Oswald'	=> 'Oswald',
			'Lato'		=> 'Lato',
			'Source Sans Pro' => 'Source Sans Pro',
			'PT Sans'	=> 'PT Sans',
			'Open Sans Condensed' => 'Open Sans Condensed',
			'Droid Sans' => 'Droid Sans',
			'Montserrat' => 'Montserrat',
			'Merriweather' => 'Merriweather',
			'Lora'		=> 'Lora',
			'Arimo'		=> 'Arimo',
			'Bitter'	=> 'Bitter',
			'Lobster'	=> 'Lobster',
			'Indie Flower' => 'Indie Flower',
			'Oxygen'	=> 'Oxygen'
        ),
    )
);	    

//Content Font

$wp_customize->add_setting( 'font_content', array(
	        'default' => 'Open Sans'
	       
	    ) );
	    
 $wp_customize->add_control(
    'font_content',
    array(
        'type' => 'select',
        'label' => 'Content Font',
        'section' => 'font',
        'choices' => array(
            'Times New Roman' => 'Times New Roman',
            'Arial'     => 'Arial',
            'Courier New'   => 'Courier New',
			'Open Sans' => 'Open Sans',
			'Slabo'		=> 'Slabo',
			'Roboto'	=> 'Roboto',
			'Oswald'	=> 'Oswald',
			'Lato'		=> 'Lato',
			'Source Sans Pro' => 'Source Sans Pro',
			'PT Sans'	=> 'PT Sans',
			'Open Sans Condensed' => 'Open Sans Condensed',
			'Droid Sans' => 'Droid Sans',
			'Montserrat' => 'Montserrat',
			'Merriweather' => 'Merriweather',
			'Lora'		=> 'Lora',
			'Arimo'		=> 'Arimo',
			'Bitter'	=> 'Bitter',
			'Lobster'	=> 'Lobster',
			'Indie Flower' => 'Indie Flower',
			'Oxygen'	=> 'Oxygen'
        ),
    )
);	    


/**----------------------------------------------------------------
* 
* Logo and Favicon Section
* 
-------------------------------------------------------------------*/
	$wp_customize->add_section( 'logo_favicon_section' , array(
    'title'       => __( 'Logo & Favicon', 'wpt' ),
    'description' => 'Upload a Logo and a Favicon to be displayed in your theme',
    'priority' => 2
) );

// Logo
	$wp_customize->add_setting( 'wpt_logo',
	    array ( 'default' => '',
	    'sanitize_callback' => 'esc_url_raw'
	 ));
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'wpt_logo', array(
    'label'    => __( 'Logo', 'wpt' ),
    'section'  => 'logo_favicon_section',
    'settings' => 'wpt_logo',
) ) );   

// Favicon
    $wp_customize->add_setting( 'wpt_favicon',
        array ( 'default' => '',
	    'sanitize_callback' => 'esc_url_raw'
    ));
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'wpt_favicon', array(
    'label'    => __( 'Favicon', 'wpt' ),
    'section'  => 'logo_favicon_section',
    'settings' => 'wpt_favicon',
) ) );   
   
/**--------------------------------------------------------------------
* 
* General
* 
-----------------------------------------------------------------------*/

$wp_customize->add_section( 'general_section' , array(
    'title'       => __( 'General', 'wpt' ),
    'description' => 'Set general settings from here.',
    'priority' => 5
) );

$wp_customize->add_setting( 'hide_comments',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
     ));
     
$wp_customize->add_control( 'hide_comments', array(
    'type' => 'checkbox',
    'label' => 'Hide Comments',
    'section' => 'general_section',
    'description' => 'Check this to hide WordPress commenting system',
));

$wp_customize->add_setting( 'hide_author_bio',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
     ));
     
$wp_customize->add_control( 'hide_author_bio', array(
    'type' => 'checkbox',
    'label' => 'Hide Author Bio',
    'section' => 'general_section',
    'description' => 'Check this to hide author biography under post content',
));

$wp_customize->add_setting( 'hide_breadcrumbs_from_bar',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
     ));
     
$wp_customize->add_control( 'hide_breadcrumbs_from_bar', array(
    'type' => 'checkbox',
    'label' => 'Hide Breadcrumbs',
    'section' => 'general_section',
    'description' => 'Check this to hide breadcrumbs',
));

$wp_customize->add_setting( 'footer_text', 
	array(
	 'default' => '',
	 'sanitize_callback' => 'wpt_sanitize_textarea',
	 'capability' => 'edit_theme_options'
	 ));
	 
$wp_customize->add_control( 'footer_text', array(
 'label' => 'Footer Text',
 'section' => 'general_section',
 'type' => 'textarea'
 ) );

/**--------------------------------------------------------------------
* 
* Slider Section
* 
-----------------------------------------------------------------------*/

$wp_customize->add_section( 'slider_section' , array(
    'title'       => __( 'Slider Section', 'wpt' ),
    'description' => 'Upload images to be displayed in home page slider',
    'priority' => 6
) );


// Slider 1

$wp_customize->add_setting( 'slider_image_1',
  array ( 'default' => '',
    'sanitize_callback' => 'esc_url_raw'
  ));
  
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slider_image_1', array(
    'label'    => __( 'Image 1', 'wpt' ),
    'section'  => 'slider_section',
    'settings' => 'slider_image_1',
) ) );   

$wp_customize->add_setting( 'slider_title_1', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'slider_title_1', array(
 'label' => 'Title 1',
 'section' => 'slider_section',
 'type' => 'text'
 ) );
 $wp_customize->add_setting( 'slider_content_1', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'slider_content_1', array(
 'label' => 'Content 1',
 'section' => 'slider_section',
 'type' => 'text'
 ) );
  $wp_customize->add_setting( 'button_text_1', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'button_text_1', array(
 'label' => 'Button Text 1',
 'section' => 'slider_section',
 'type' => 'text'
 ) );
  $wp_customize->add_setting( 'button_link_1', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'button_link_1', array(
 'label' => 'Button Link 1',
 'section' => 'slider_section',
 'type' => 'text'
 ) );


// Slider 2

$wp_customize->add_setting( 'slider_image_2',
  array ( 'default' => '',
    'sanitize_callback' => 'esc_url_raw'
  ));
  
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slider_image_2', array(
    'label'    => __( 'Image 2', 'wpt' ),
    'section'  => 'slider_section',
    'settings' => 'slider_image_2',
) ) );  

$wp_customize->add_setting( 'slider_title_2', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'slider_title_2', array(
 'label' => 'Title 2',
 'section' => 'slider_section',
 'type' => 'text'
 ) );
 $wp_customize->add_setting( 'slider_content_2', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'slider_content_2', array(
 'label' => 'Content 2',
 'section' => 'slider_section',
 'type' => 'text'
 ) );
  $wp_customize->add_setting( 'button_text_2', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'button_text_2', array(
 'label' => 'Button Text 2',
 'section' => 'slider_section',
 'type' => 'text'
 ) );
  $wp_customize->add_setting( 'button_link_2', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'button_link_2', array(
 'label' => 'Button Link 2',
 'section' => 'slider_section',
 'type' => 'text'
 ) );


// Slider 3

$wp_customize->add_setting( 'slider_image_3',
  array ( 'default' => '',
    'sanitize_callback' => 'esc_url_raw'
  ));
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slider_image_3', array(
    'label'    => __( 'Image 3', 'wpt' ),
    'section'  => 'slider_section',
    'settings' => 'slider_image_3',
) ) );  

$wp_customize->add_setting( 'slider_title_3', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'slider_title_3', array(
 'label' => 'Title 3',
 'section' => 'slider_section',
 'type' => 'text'
 ) );
 $wp_customize->add_setting( 'slider_content_3', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'slider_content_3', array(
 'label' => 'Content 3',
 'section' => 'slider_section',
 'type' => 'text'
 ) );
  $wp_customize->add_setting( 'button_text_3', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'button_text_3', array(
 'label' => 'Button Text 3',
 'section' => 'slider_section',
 'type' => 'text'
 ) );
  $wp_customize->add_setting( 'button_link_3', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'button_link_3', array(
 'label' => 'Button Link 3',
 'section' => 'slider_section',
 'type' => 'text'
 ) );

/**--------------------------------------------------------------------
* 
* Home Page Section
* 
-----------------------------------------------------------------------*/

$wp_customize->add_section( 'hp_section' , array(
    'title'       => __( 'Home Page Section', 'wpt' ),
    'description' => 'Set Home Page Contents from here.',
    'priority' => 7
) );


// checkbox
$wp_customize->add_setting( 'hp_checkbox',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
));

$wp_customize->add_control( 'hp_checkbox', array(
    'type' => 'checkbox',
    'label' => 'Show Home Page Section',
    'section' => 'hp_section',
    'description' => 'Check this to enable Home Page Section',
));

$wp_customize->add_setting( 'hp_title', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ));
$wp_customize->add_control( 'hp_title', array(
 'label' => 'Title',
 'section' => 'hp_section',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'hp_content', array(
 'default' => '',
 'sanitize_callback' => 'wpt_sanitize_textarea',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'hp_content', array(
 'label' => 'Content',
 'section' => 'hp_section',
 'type' => 'textarea'
 ) );

$wp_customize->add_setting( 'hp_subsection_1', array(
 'default' => '',
 'sanitize_callback' => 'wpt_sanitize_textarea',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'hp_subsection_1', array(
 'label' => 'Sub-Section 1',
 'section' => 'hp_section',
 'type' => 'textarea'
 ) );

$wp_customize->add_setting( 'hp_subsection_2', array(
 'default' => '',
 'sanitize_callback' => 'wpt_sanitize_textarea',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'hp_subsection_2', array(
 'label' => 'Sub-Section 2',
 'section' => 'hp_section',
 'type' => 'textarea'
 ) );

$wp_customize->add_setting( 'hp_subsection_3', array(
 'default' => '',
 'sanitize_callback' => 'wpt_sanitize_textarea',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'hp_subsection_3', array(
 'label' => 'Sub-Section 3',
 'section' => 'hp_section',
 'type' => 'textarea'
 ) );

/**--------------------------------------------------------------------
* 
* Counter Section
* 
-----------------------------------------------------------------------*/

$wp_customize->add_section( 'counter_section' , array(
    'title'       => __( 'Counter Section', 'wpt' ),
    'description' => 'Set Counter Section Contents from here.',
    'priority' => 8
) );

// checkbox 1
$wp_customize->add_setting( 'counter_checkbox_1',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
));
$wp_customize->add_control( 'counter_checkbox_1', array(
    'type' => 'checkbox',
    'label' => 'Counter Section 1',
    'section' => 'counter_section',
    'description' => 'Check this to enable Counter Section 1',
));

$wp_customize->add_setting( 'counter_title_1', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'counter_title_1', array(
 'label' => 'Title 1',
 'section' => 'counter_section',
 'type' => 'text'
 ) );
 
 $wp_customize->add_setting( 'counter_count_1', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'counter_count_1', array(
 'label' => 'Count 1',
 'section' => 'counter_section',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'counter_image_1',
  array ( 'default' => '',
    'sanitize_callback' => 'esc_url_raw'
  ));
  
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'counter_image_1', array(
    'label'    => __( 'Image 1', 'wpt' ),
    'section'  => 'counter_section',
    'settings' => 'counter_image_1',
) ) );  

// checkbox 2
$wp_customize->add_setting( 'counter_checkbox_2',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
));
$wp_customize->add_control( 'counter_checkbox_2', array(
    'type' => 'checkbox',
    'label' => 'Counter Section 2',
    'section' => 'counter_section',
    'description' => 'Check this to enable Counter Section 2',
));

$wp_customize->add_setting( 'counter_title_2', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'counter_title_2', array(
 'label' => 'Title 2',
 'section' => 'counter_section',
 'type' => 'text'
 ) );
 
 $wp_customize->add_setting( 'counter_count_2', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'counter_count_2', array(
 'label' => 'Count 2',
 'section' => 'counter_section',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'counter_image_2',
  array ( 'default' => '',
    'sanitize_callback' => 'esc_url_raw'
  ));
  
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'counter_image_2', array(
    'label'    => __( 'Image 2', 'wpt' ),
    'section'  => 'counter_section',
    'settings' => 'counter_image_2',
) ) );  

// checkbox 3
$wp_customize->add_setting( 'counter_checkbox_3',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
));

$wp_customize->add_control( 'counter_checkbox_3', array(
    'type' => 'checkbox',
    'label' => 'Counter Section 3',
    'section' => 'counter_section',
    'description' => 'Check this to enable Counter Section 3',
));

$wp_customize->add_setting( 'counter_title_3', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'counter_title_3', array(
 'label' => 'Title 3',
 'section' => 'counter_section',
 'type' => 'text'
 ) );
 
 $wp_customize->add_setting( 'counter_count_3', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'counter_count_3', array(
 'label' => 'Count 3',
 'section' => 'counter_section',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'counter_image_3',
  array ( 'default' => '',
    'sanitize_callback' => 'esc_url_raw'
  ));
  
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'counter_image_3', array(
    'label'    => __( 'Image 3', 'wpt' ),
    'section'  => 'counter_section',
    'settings' => 'counter_image_3',
) ) );  

// checkbox 4
$wp_customize->add_setting( 'counter_checkbox_4',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
));
$wp_customize->add_control( 'counter_checkbox_4', array(
    'type' => 'checkbox',
    'label' => 'Counter Section 4',
    'section' => 'counter_section',
    'description' => 'Check this to enable Counter Section 4',
));

$wp_customize->add_setting( 'counter_title_4', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'counter_title_4', array(
 'label' => 'Title 4',
 'section' => 'counter_section',
 'type' => 'text'
 ) );
 
 $wp_customize->add_setting( 'counter_count_4', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'counter_count_4', array(
 'label' => 'Count 4',
 'section' => 'counter_section',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'counter_image_4',
  array ( 'default' => '',
    'sanitize_callback' => 'esc_url_raw'
  ));
  
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'counter_image_4', array(
    'label'    => __( 'Image 4', 'wpt' ),
    'section'  => 'counter_section',
    'settings' => 'counter_image_4',
) ) );  

/**----------------------------------------------------------------------------
*
* Social icons
*
------------------------------------------------------------------------------*/

$wp_customize -> add_section( 'social_icon_link', array(
	'title' => __( 'Social Icons','wpt' ),
	'description' => __('Type your social links here','wpt'),
	'priority' => 9
));

 
$wp_customize->add_setting( 'twitter_url', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
 $wp_customize->add_control( 'twitter_url', array(
 'label' => 'Twitter Profile',
 'section' => 'social_icon_link',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'facebook_url', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
 $wp_customize->add_control( 'facebook_url', array(
 'label' => 'Facebook Profile',
 'section' => 'social_icon_link',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'googleplus_url', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
 $wp_customize->add_control( 'googleplus_url', array(
 'label' => 'Google+ Profile',
 'section' => 'social_icon_link',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'linkedin_url', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
 $wp_customize->add_control( 'linkedin_url', array(
 'label' => 'LinkedIn Profile',
 'section' => 'social_icon_link',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'pinterest_url', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
 $wp_customize->add_control( 'pinterest_url', array(
 'label' => 'Pinterest Profile',
 'section' => 'social_icon_link',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'blogger_url', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
 $wp_customize->add_control( 'blogger_url', array(
 'label' => 'Blogger Profile',
 'section' => 'social_icon_link',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'youtube_url', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
 $wp_customize->add_control( 'youtube_url', array(
 'label' => 'Youtube Profile',
 'section' => 'social_icon_link',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'flikr_url', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
 $wp_customize->add_control( 'flikr_url', array(
 'label' => 'Flickr Profile',
 'section' => 'social_icon_link',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'vimeo_url', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
 $wp_customize->add_control( 'vimeo_url', array(
 'label' => 'Vimeo Profile',
 'section' => 'social_icon_link',
 'type' => 'text'
 ) );

/*----------------------------------------------------------------------------
*
* Contact Us Section
*
------------------------------------------------------------------------------*/

$wp_customize -> add_section( 'contact_us_section', array(
	'title' => __( 'Contact Us Information','wpt' ),
	'description' => __('Type your contact information here','wpt'),
	'priority' => 10
));

// address
$wp_customize->add_setting( 'address_checkbox',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
));

$wp_customize->add_control( 'address_checkbox', array(
    'type' => 'checkbox',
    'label' => 'Show Location Information',
    'section' => 'contact_us_section',
    'description' => 'Check this to show your address',
));

$wp_customize->add_setting( 'address_title', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'address_title', array(
 'label' => 'Title',
 'section' => 'contact_us_section',
 'type' => 'text'
 ) );

$wp_customize->add_setting( 'address_content', array(
 'default' => '',
 'sanitize_callback' => 'wpt_sanitize_textarea',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'address_content', array(
 'label' => 'Content',
 'section' => 'contact_us_section',
 'type' => 'textarea'
 ) );
 
// contact details
$wp_customize->add_setting( 'contact_checkbox',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
));

$wp_customize->add_control( 'contact_checkbox', array(
    'type' => 'checkbox',
    'label' => 'Show Contact Information',
    'section' => 'contact_us_section',
    'description' => 'Check this to show your contact details',
));

$wp_customize->add_setting( 'contact_title', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'contact_title', array(
 'label' => 'Title',
 'section' => 'contact_us_section',
 'type' => 'text'
 ) );

$wp_customize->add_setting( 'email_id', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'email_id', array(
 'label' => 'Email:',
 'section' => 'contact_us_section',
 'type' => 'text'
 ) );

$wp_customize->add_setting( 'primary_ph', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'primary_ph', array(
 'label' => 'Primary Phone Number',
 'section' => 'contact_us_section',
 'type' => 'text'
 ) );
 
 $wp_customize->add_setting( 'alternate_ph', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'alternate_ph', array(
 'label' => 'Alternate Phone Number',
 'section' => 'contact_us_section',
 'type' => 'text'
 ) );
 
 $wp_customize->add_setting( 'fax_no', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'fax_no', array(
 'label' => 'Fax Number',
 'section' => 'contact_us_section',
 'type' => 'text'
 ) );

// office hours
$wp_customize->add_setting( 'timing_checkbox',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
));
$wp_customize->add_control( 'timing_checkbox', array(
    'type' => 'checkbox',
    'label' => 'Show Office Hours Information',
    'section' => 'contact_us_section',
    'description' => 'Check this to show your office hours details',
));

$wp_customize->add_setting( 'timing_title', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'timing_title', array(
 'label' => 'Title',
 'section' => 'contact_us_section',
 'type' => 'text'
 ) );

$wp_customize->add_setting( 'timing_content', array(
 'default' => '',
 'sanitize_callback' => 'wpt_sanitize_textarea',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'timing_content', array(
 'label' => 'Content',
 'section' => 'contact_us_section',
 'type' => 'textarea'
 ) );

// map
$wp_customize->add_setting( 'map_checkbox',
     array(
        'default' => '',
        'sanitize_callback' => 'wpt_sanitize_checkbox_field',
));

$wp_customize->add_control( 'map_checkbox', array(
    'type' => 'checkbox',
    'label' => 'Show Map Information',
    'section' => 'contact_us_section',
    'description' => 'Check this to show Map',
));

$wp_customize->add_setting( 'latitude', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'latitude', array(
 'label' => 'Latitude',
 'section' => 'contact_us_section',
 'type' => 'text'
 ) );

$wp_customize->add_setting( 'longitude', array(
 'default' => '',
 'sanitize_callback' => 'sanitize_text_field',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'longitude', array(
 'label' => 'Longitude',
 'section' => 'contact_us_section',
 'type' => 'text'
 ) );
 
$wp_customize->add_setting( 'marker_image',
  array ( 'default' => '',
    'sanitize_callback' => 'esc_url_raw'
  ));
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'marker_image', array(
    'label'    => __( 'Marker', 'wpt' ),
    'section'  => 'contact_us_section',
    'settings' => 'marker_image',
) ) );  

$wp_customize->add_setting( 'marker_detail', array(
 'default' => '',
 'sanitize_callback' => 'wpt_sanitize_textarea',
 'capability' => 'edit_theme_options'
 ) );
$wp_customize->add_control( 'marker_detail', array(
 'label' => 'Map Pop-Up Content',
 'section' => 'contact_us_section',
 'type' => 'textarea'
 ) );

}
add_action( 'customize_register', 'wpt_customize_register' );

if ( ! function_exists( 'wpt_sanitize_checkbox_field' ) ) :
/**
 * Sanitization callback for checkbox field.
 *
 * @param string $input of checkbox .
 * @return string if checkbox input is a 1 returns one and If the input is anything else at all, the function returns a blank string .
 */
function wpt_sanitize_checkbox_field( $input ) {
	if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}
endif; // wpt_sanitize_checkbox_field

if ( ! function_exists( 'wpt_sanitize_textarea' ) ) :
/**
 * Sanitization callback for textarea field.
 *
 * @param string $text of textarea .
 * @return string 
 */
function wpt_sanitize_textarea( $text ) {
	return esc_textarea( $text );
}
endif; // wpt_sanitize_textarea 

//Add Customise css into header part
if ( ! function_exists( 'wpt_customizer_css' ) ) :
function wpt_customizer_css() {
	
   require get_template_directory() . '/inc/customize-style.php';
}
endif; // wpt_customizer_css
add_action( 'wp_head', 'wpt_customizer_css' );
?>