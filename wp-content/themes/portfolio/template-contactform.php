<?php

/**
 * Template Name: Contact Form
 * Description: A Page Template that display Contact Form.
 *
 * @package Portfolio
 * @author Purplethemes
 */
 
get_header();

$marker_img_url =  get_theme_mod( 'marker_image' );
$marker_latitude = get_theme_mod( 'latitude' );
$marker_longitude = get_theme_mod( 'longitude' );
$marker_text =  get_theme_mod( 'marker_detail' );

$post_id = $wp_query->get_queried_object_id(); 
$args=array(
  'page_id' => $post_id,
  'post_type' => 'page',
  'post_status' => 'publish',
  'posts_per_page' => 1,
  'caller_get_posts'=> 1
);
$myposts = get_posts( $args );

if (get_theme_mod( 'address_checkbox' ) == '' && get_theme_mod( 'contact_checkbox' ) == ''&& get_theme_mod( 'timing_checkbox' ) == '')
	{
		$right_class = '';
		$left_class = 'col-xs-12 col-sm-12 col-md-12';
	}
	
else
	{
		
		$right_class = 'col-xs-12 col-sm-4 col-md-4';
		$left_class = 'col-xs-12 col-sm-8 col-md-8';
	}

foreach ( $myposts as $post ) : setup_postdata( $post );

?>

<div class="container">
    <section class="contact-section">
        <section class="row">
            <article class="<?php echo $left_class; ?>">
            <!-- contact form section -->
                <div class="contact-form wow fadeInLeft" data-wow-delay="0.2s">
                    <h3><?php the_title(); ?></h3>
                    <div class="title-divider"></div>
                    <p><?php echo get_the_content(); ?></p>
                   
                </div>
            <!-- contact form section end -->
            </article>
            
            <article class="<?php echo $right_class; ?>">
            	<div class="contact-info">
                	<?php if( get_theme_mod( 'address_checkbox' ) != '' )
		                	{ ?>
								<div class="office-location wow fadeInRight" data-wow-delay="0.3s">
		                        	<h3><?php echo get_theme_mod( 'address_title' )? get_theme_mod( 'address_title' ):'Address'  ?></h3>
		                        	<div class="title-divider"></div>
		                        	
		                        	<?php 
		                        	if( get_theme_mod( 'address_content' ) != '' ) {
				              			
				              			echo '<p>' .get_theme_mod( 'address_content' ). '</p>';
											
									}
					              	?>
					               	
		              				
		                    	</div>
							<?php }
					
		                   	if( get_theme_mod( 'contact_checkbox' ) != '' )
		                    { ?>
								<div class="office-info wow fadeInRight" data-wow-delay="0.6s">
			                        <h3><?php echo get_theme_mod( 'contact_title' )? get_theme_mod( 'contact_title' ):'Contact Details'  ?></h3>
			                        <div class="title-divider"></div>
			                        
			                        <div class="row">
			                        	<div class="col-xs-12 col-sm-12 col-md-12">
			                            	<div class="row">
						                        <?php if( get_theme_mod('email_id') != '')
						                        		{
						                                    echo '<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo '<div class="info-title">Email:</div>';
						                                    echo '</div>';
						                                    echo '<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo '<div class="info"><a href="mailto:'.get_theme_mod('email_id').'">'.get_theme_mod('email_id').'</a></div>';
						                                   		echo '</div>';
													} 
												
												 	if( get_theme_mod('primary_ph') != '')
						                       		 	{   
						                            
						                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo'<div class="info-title">Primary Phone:</div>';
						                                    echo'</div>';
						                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo'<div class="info"><a href="tel:'.get_theme_mod('primary_ph').'">'.get_theme_mod('primary_ph').'</a></div>';
						                                    echo'</div>';
						                                }    
						                         
						                         	if( get_theme_mod('alternate_ph') != '')
						                       			{   
						                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo'<div class="info-title">Alternate Phone:</div>';
						                                    echo'</div>';
						                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo'<div class="info"><a href="tel:'.get_theme_mod('alternate_ph').'">'.get_theme_mod('alternate_ph').'</a></div>';
						                                    echo'</div>';
						                            
						                        		}   
						                        
						                        	if( get_theme_mod('fax_no') != '' )
						                        		{  
						                            
						                                     echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                         echo'<div class="info-title">Fax Number:</div>';
						                                     echo'</div>';
						                                     echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                         echo'<div class="info"><a href="tel:'.get_theme_mod('fax_no').'">'.get_theme_mod('fax_no').'</a></div>';   
						                                     echo'</div>';
						                                 
						                        		} ?>
			                        		</div>
			                        	</div>
			                        </div>
			                        
		                    	</div>
							<?php }
                    
		                    if( get_theme_mod( 'timing_checkbox' ) != '' )
		                    { ?>
			                    <div class="office-time wow fadeInRight" data-wow-delay="1s">
			                        <h3><?php echo get_theme_mod( 'timing_title' )? get_theme_mod( 'timing_title' ):'Office Hours'  ?></h3>
			                        <div class="title-divider"></div>
			                        
			                        <?php 
		                        	if( get_theme_mod( 'timing_content' ) != '' ) {
				              			
				              			echo '<p>' .get_theme_mod( 'timing_content' ). '</p>';
											
									}
					              	?>
			                        
			                    </div>
			                <?php } ?>
                </div>
            </article>
        </section>
    </section>
</div>

<!-- google map section -->
<?php if( get_theme_mod( 'map_checkbox' ) != '' ) { ?>
	<div id="map_canvas"></div>
<?php } ?>
   
<!-- google map section end -->



<!-- google map API section -->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;language=en"></script>
<script type="text/javascript">

function initialize() {
    var image = "<?php echo get_theme_mod( 'marker_image' ); ?>";
    var mapOptions = {
        zoom: 13,
        center: new google.maps.LatLng("<?php echo get_theme_mod( 'latitude' ); ?>", "<?php echo get_theme_mod( 'longitude' ); ?>"),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    var myPos = new google.maps.LatLng("<?php echo get_theme_mod( 'latitude' ); ?>", "<?php echo get_theme_mod( 'longitude' ); ?>");
    
    var myMarker = new google.maps.Marker({
        position: myPos,
        map: map,
        draggable: false,
        icon: image
    });
    
	var infowindow =  new google.maps.InfoWindow({
		content: ''
	});
		
	bindInfoWindow(myMarker, map, infowindow, "<?php echo get_theme_mod( 'marker_detail' ); ?>");

}

google.maps.event.addDomListener(window, 'resize', initialize);
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<!-- google map API section end -->


<?php 
endforeach; 
wp_reset_postdata();
get_footer(); ?>