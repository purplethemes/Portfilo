<?php
/**
 * Header functions.
 * @package Portfolio
 * @author Purplethemes
 * 
 */
global $portfolio_options;
 
/* ---------------------------------------------------------------------------
 * Styles
 * --------------------------------------------------------------------------- */

if ( ! function_exists( 'portfolio_styles' ) ) :

function portfolio_styles() 
{
	// wp_enqueue_style ------------------------------------------------------
   
    /* css section  */
    
	echo '<link rel="stylesheet" href="'. THEME_URI .'/css/bootstrap.css?ver='.THEME_VERSION.'" media="all" />'."\n";
	
	echo '<link rel="stylesheet" href="'. THEME_URI .'/css/lightbox.css?ver='.THEME_VERSION.'" media="all" />'."\n";
	
    echo '<link rel="stylesheet" href="'. THEME_URI .'/css/jquery.bxslider.css?ver='.THEME_VERSION.'" media="all" />'."\n";
    echo '<link rel="stylesheet" href="'. THEME_URI .'/css/font-awesome.min.css?ver='.THEME_VERSION.'" media="all" />'."\n";
    echo '<link rel="stylesheet" href="'. THEME_URI .'/css/animate.css?ver='.THEME_VERSION.'" media="all" />'."\n";
    
    // Load our custom main stylesheet.
    echo '<link rel="stylesheet" href="'. THEME_URI .'/css/custom.css?ver='.THEME_VERSION.'" media="all" />'."\n";
   
    // Load our wordpress default stylesheet.
	wp_enqueue_style( 'wpt-style', get_stylesheet_uri() );
	
	
    /* css section end  */

	/* Google Fonts ---------------------------------------------------------- */
	
	echo '<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" type="text/css" >'."\n";
		
    /* google font section end */    
    
    
    // Custom CSS ------------------------------------------------------------
    
    global $portfolio_options;
    
    $portfolio_custom_css = $portfolio_options['custom_css'];
	if( $portfolio_custom_css )
    {
		echo '<style>'."\n";
		echo $portfolio_custom_css."\n";
		echo '</style>'."\n";
	}
    
    /* Custom css end */
    
    
}
endif; // portfolio_styles

add_action('wp_styles', 'portfolio_styles');

/* ---------------------------------------------------------------------------
 * IE fix
 * --------------------------------------------------------------------------- */

if ( ! function_exists( 'portfolio_ie_fix' ) ) :
 
function portfolio_ie_fix() 
{
	if( ! is_admin() )
	{
		echo "\n".'<!--[if lt IE 9]>'."\n";
		echo '<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>'."\n";
        echo '<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>'."\n";
		echo '<![endif]-->'."\n";
		
	}	
}
endif; //portfolio_ie_fix
add_action('wp_head', 'portfolio_ie_fix');

/* ---------------------------------------------------------------------------
 * Scripts
 * --------------------------------------------------------------------------- */
 
if ( ! function_exists( 'portfolio_scripts' ) ) :
 
function portfolio_scripts() 
{
	if( ! is_admin() ) 
	{
	    /* included javascript section */
		
		wp_enqueue_script('jquery');
		
		wp_enqueue_script( 'jquery-bootstrap-js', THEME_URI .'/js/bootstrap.min.js', false, THEME_VERSION, true );
		
		wp_enqueue_script( 'jquery-ai-menu', THEME_URI. '/js/ai-menu.js', false, THEME_VERSION, true );
		
		wp_enqueue_script( 'jquery-form-min', THEME_URI .'/js/jquery.form.js', false, THEME_VERSION, true  );
		
		wp_enqueue_script( 'jquery-flexslider-min', THEME_URI. '/js/jquery.flexslider-min.js', false, THEME_VERSION, true );
		
		wp_enqueue_script( 'jquery-bxslider-min', THEME_URI. '/js/jquery.bxslider.min.js', false, THEME_VERSION, true );
		
		wp_enqueue_script( 'jquery-lightbox-min', THEME_URI. '/js/lightbox.min.js', false, THEME_VERSION, true );
		
        wp_enqueue_script( 'jquery-counterup-min', THEME_URI. '/js/jquery.counterup.min.js', false, THEME_VERSION, true );
        
		wp_enqueue_script( 'jquery-nicescroll', THEME_URI. '/js/jquery.nicescroll.min.js', false, THEME_VERSION, true  );
		
		wp_enqueue_script( 'jquery-validate', THEME_URI. '/js/jquery.validate.js', false, THEME_VERSION);
		
		wp_enqueue_script( 'jquery-wow-min', THEME_URI. '/js/wow.min.js', false, THEME_VERSION, true );
		
		wp_enqueue_script( 'jquery-main', THEME_URI. '/js/main.js', false, THEME_VERSION, true );
				
	}
    /* included javascript section end */
}
endif; //portfolio_scripts
add_action('wp_enqueue_scripts', 'portfolio_scripts');


/* ---------------------------------------------------------------------------
 * Portfolio logo
* --------------------------------------------------------------------------- */

if ( ! function_exists( 'portfolio_logo' ) ) :

function portfolio_logo()
{

    
    if ( get_theme_mod( 'wpt_logo' ) ) {
		echo '<a href="'.SITE_URL.'" class="navbar-brand">';
        echo '<img src="'.esc_url( get_theme_mod( 'wpt_logo' ) ).'" alt="Portfilo" title="Portfilo" />';
        echo '</a>';
	}
   
 else {
 	   echo '<a href="'.SITE_URL.'" class="navbar-brand">';
       echo '<img src="'.THEME_URI.'/images/logo_new.png" alt="Portfilo" title="Portfilo" />';
       echo '</a>';
 	}
   
}
endif; //portfolio_logo

if ( ! function_exists( 'site_maintenance_content' ) ) :

function site_maintenance_content() {
	if(!is_user_logged_in() || is_user_logged_in())
	{
		global $data;
		if($data['emergency_shutdown']== 'Yes')
		{
			include get_template_directory(). '/maintenance.php';
			
			die;
		}
	}
}
endif; //site_maintenance_content
add_action('get_header', 'site_maintenance_content');
?>