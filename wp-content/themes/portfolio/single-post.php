<?php
/** 
 * The Template for displaying all single posts.
 * 
 * @package Portfolio
 * @author Purplethemes
 */
 
get_header();

global $wp_query;
$post_id = $wp_query->get_queried_object_id();
$lt = get_post_meta($post_id, 'Layout', true);

switch ( get_post_meta($post_id, 'Layout', true) ) {
	
	case 'left_sidebar':
		$class = 'left';
        break;
	case 'right_sidebar':
		$class = 'right';
		break;
	case 'both_sidebar':
		$class = 'both';
		break;
	default:
		$class = 'full';
		break;
}
 
if($class == 'left'){
  
    $right_class = 'col-xs-12 col-sm-8 col-md-8 pull-right';
    $left_class = 'col-xs-12 col-sm-4 col-md-4 pull-left';
    $class = 'left';
}
     
elseif($class == 'right'){
    
    $right_class = 'col-xs-12 col-sm-8 col-md-8';
    $left_class = 'col-xs-12 col-sm-4 col-md-4';
    $class = 'right';
}

elseif($class == 'both'){
    
    $center_class = 'col-xs-12 col-sm-6 col-md-6';
    $left_class = 'col-xs-12 col-sm-3 col-md-3';
    $class = 'both';
    
} 

else{
	
	$right_class = '';
	$class = 'full';
}
 
?>
 
 <div class="container">
    <section class="news-section">
        <section class="row">
        	<?php if($class) {
			
				if($class == 'both') {
				
					echo '<article class="' .$left_class. '">';
	                            echo'<aside>';
	                                    echo'<div class="sidebar">';
	                                            get_sidebar();
	                                    echo '</div>';
	                            echo '</aside>';
	                    echo'</article>';
	                    
	                echo'<article class="' .$center_class.'">';
								echo '<div class="news-list">';
	                    				while ( have_posts() ) {
						                			
						                	the_post();
	                    					get_template_part( 'libs/content', 'blog' );
	                    				}
	                    					
	                    		echo '</div>';
	                    					
						echo '</article>';
	                
				} 
				
				else {
				
					echo'<article class="' .$right_class.'">';
												
		                    	echo '<div class="news-list">';
		                    						
		                    				get_template_part( 'libs/content', 'blog' );
		                    					
		                    	echo '</div>';
		                    					
						echo '</article>';
				}
			
			}				
			
			if($class){ 
			
					if($class == 'full') {
					
						echo '';
					}
					
					else {
						
						echo '<article class="' .$left_class. '">';
	                              echo'<aside>';
	                                    echo'<div class="sidebar">';
	                                            include get_template_directory(). '/sidebar-blog.php';
	                                            
	                                   echo '</div>';
	                              echo '</aside>';
	                    echo'</article>';
					}
	                    
        
                } 
        ?>
		</section>
    </section>
</div>

<?php  get_footer(); ?>