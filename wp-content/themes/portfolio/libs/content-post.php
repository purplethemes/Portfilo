<?php
/**
 * The template for displaying blog posts.
 *
 * @package Portfolio
 * @author Purplethemes
 */ 
global $wp_query;
$post_id = $wp_query->get_queried_object_id();
$blog_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'blog-listing-thumbnail' ) ); 

?>
 
 <figure class="news wow fadeInLeft" data-wow-delay="0.3s">
        <div class="col-xs-12 col-sm-5 col-md-5">
            <div class="news-img">
                <a href="<?php the_permalink(); ?>">
                	<?php if(!empty($blog_url)) { ?>
                    	<img class="img-responsive" src="<?php echo $blog_url; ?>" alt="<?php _e('News Title', 'wpt'); ?>" />
                    <?php } else { ?>
						<img class="img-responsive" src="<?php echo THEME_URI. '/images/no-img-portfolio.jpg'; ?>" alt="<?php _e('News Title', 'wpt'); ?>"  style="width:458px;height:305px;" /> 
					<?php } ?>
                </a>
            </div>
        </div>
                       
        <figure class="col-xs-12 col-sm-7 col-md-7">
            <figcaption class="news-info">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <div class="meta">
                   <span class="date">
                     <i class="glyphicon glyphicon-calendar"></i>
                     <?php echo get_the_date(); ?>
                   </span>
                   <span class="category">
                     <i class="glyphicon glyphicon-tag"></i> 
                	 <?php 
                	 $tags_list = get_the_tag_list( '', _x( ', ', 'Used between list items, there is a space after the comma.', 'wpt' ) ); 
                	 if ( $tags_list ) {
					  printf($tags_list);
				     }?>
				   </span> 
                </div>
                <?php the_excerpt(); ?>
               
                <a class="more-news" href="<?php the_permalink(); ?>"><?php _e( "Find Out More","wpt" )?></a> 
            </figcaption>
        </figure>
</figure>
<div class="divider"></div>