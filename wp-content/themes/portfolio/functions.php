<?php
/**
   Portfoliotheme functions and definitions
 * @package Portfolio
 * @author Purplethemes
 */

define('THEME_DIR', get_template_directory());
define('THEME_URI', get_template_directory_uri());
define('SITE_URL', site_url());

 
define('LIBS_DIR', THEME_DIR. '/inc');
define('LIBS_URI', THEME_URI. '/inc');
define('LANG_DIR', THEME_DIR. '/languages');

define('THEME_VERSION', '1.0');

add_filter('widget_text', 'do_shortcode');

/* ---------------------------------------------------------------------------
 * Loads Theme Textdomain
 * --------------------------------------------------------------------------- */
load_theme_textdomain( 'wpt', LANG_DIR );

/* ---------------------------------------------------------------------------
 * Loads Theme Files
* --------------------------------------------------------------------------- */
 
// Loads Theme Functions -----------------------------------------------------------------------
require_once( THEME_DIR. '/functions/theme-functions.php' );

// Load Header Theme Fucntion -----------------------------------------------------------------------
require_once( THEME_DIR. '/functions/theme-head.php' );

// Load Theme Layout -----------------------------------------------------------------------
require_once( THEME_DIR. '/functions/theme-layouts.php' );

// Load Theme Menu -------------------------------------------------------------------------
require_once( THEME_DIR. '/functions/theme-menu.php' );

// Load Theme Shortcode -------------------------------------------------------------------------
//require_once( THEME_DIR. '/functions/theme-shortcodes.php' );

// Load Theme Shortcode -------------------------------------------------------------------------
require_once( THEME_DIR. '/functions/class-tgm-plugin-activation.php' );

/**
 * Set up the content width value based on the theme's design.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 474;
}

/**
 * Portfolio theme only works in WordPress 3.6 or later.
 */
/*if ( version_compare( $GLOBALS['wp_version'], '3.6', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}
*/

/**
 * Adjust content_width value for image attachment template.
 */

if ( ! function_exists( 'portfoliotheme_content_width' ) ) :
 
 function portfoliotheme_content_width() {
	if ( is_attachment() && wp_attachment_is_image() ) {
		$GLOBALS['content_width'] = 810;
	}
}
endif; // portfoliotheme_content_width
add_action( 'template_redirect', 'portfoliotheme_content_width' );


if ( ! function_exists( 'portfoliotheme_get_featured_posts' ) ) : 

/**
 * Getter function for Featured Content Plugin.
 *
 * @return array An array of WP_Post objects.
 */

function portfoliotheme_get_featured_posts() {
	/**
	 * Filter the featured posts to return in Portfolio theme.
	 *
	 * @param array|bool $posts Array of featured posts, otherwise false.
	 */
	return apply_filters( 'portfoliotheme_get_featured_posts', array() );
}
endif; // portfoliotheme_get_featured_posts


if ( ! function_exists( 'portfoliotheme_get_featured_posts' ) ) : 
/**
 * A helper conditional function that returns a boolean value.
 * @return bool Whether there are featured posts.
 */
 
function portfoliotheme_has_featured_posts() {
	return ! is_paged() && (bool) portfoliotheme_get_featured_posts();
}
endif; // portfoliotheme_get_featured_posts


if ( ! function_exists( 'portfoliotheme_widgets_init' ) ) :

/**
 * Register three Portfolio theme widget areas.
 */
 
function portfoliotheme_widgets_init() {
	

	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'wpt' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Main sidebar.', 'wpt' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '<div class="title-divider"></div></h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'wpt' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Additional sidebar.', 'wpt' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '<div class="title-divider"></div></h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'wpt' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears in the footer section of the site.', 'wpt' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '<div class="title-divider"></div></h4>',
	) );
}

endif; // portfoliotheme_widgets_init
add_action( 'widgets_init', 'portfoliotheme_widgets_init' );

add_filter('widget_text', 'do_shortcode');



/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own Featured_Content class on or
 * before the 'setup_theme' hook.
 */
if ( ! class_exists( 'Featured_Content' ) && 'plugins.php' !== $GLOBALS['pagenow'] ) {
	//require get_template_directory() . '/inc/featured-content.php';
    
// Hide admin toolbar
add_filter('show_admin_bar', '__return_false');
}

if ( ! function_exists( 'remove_comment_fields' ) ) : 

function remove_comment_fields($fields) {
   
    unset($fields['url']);
    return $fields;
}
endif; // remove_comment_fields
add_filter('comment_form_default_fields', 'remove_comment_fields');


if ( ! function_exists( 'ai_newsletter_call' ) ) :
/*
* Newsletter function
*
*/

function ai_newsletter_call(){	

	global $wpdb;

	$arr = 1;
	
	$ai_email = $_POST['email'];

	$ai_admin_subject = $ai_email.__(' has subscribed us');

	$ai_admin_headers = "MIME-Version: 1.0\n";

	$ai_admin_headers .= "Content-type: text/html; charset=UTF-8\n";	

	$ai_admin_headers .= "From: <".$ai_email.">\n";

	$ai_admin_headers .= "Message-Id: <".time()."@".$_SERVER['SERVER_NAME'].">\n";

	$ai_admin_headers .= "X-Mailer: php-mail-function-0.2\n";

	$ai_admin_usermsg = $ai_email. ' has subscribed to our newsletter.';


	if($arr == 1) {		
		wp_mail($ai_emailadmin, $ai_admin_subject, $ai_admin_usermsg, $ai_admin_headers);	

		//Check whether mailchimpl extension for Contact plugin is active or not
		if ( is_plugin_active('responsive-contact-form-mailchimp-extension/ai-responsive-contact-form-mailchimp-extension.php' ) ) {	
			$apikey = esc_attr(get_option('ai_me_contactform_api_key'));
			$active_mail_chimp =  get_option('aimclists') ;	
			
			
			require_once( get_home_path().'wp-content/plugins/responsive-contact-form-mailchimp-extension/admin/includes/AIMCAPI.class.php');
			
			$storedata = new AIMCAPI($apikey);

			if($active_mail_chimp) {
				foreach($active_mail_chimp as $list_id => $list_val) {			
					$storedata->listSubscribe($list_id, $ai_email);
				}
				if ($storedata->errorCode) {
					echo $storedata->errorMessage;
				}
			}	
		}
	}
	echo json_encode($arr);	
	die(); 	
}
endif; // ai_newsletter_call
add_action('wp_ajax_ai_newsletter', 'ai_newsletter_call');
add_action('wp_ajax_nopriv_ai_newsletter', 'ai_newsletter_call');

remove_filter( 'the_content', 'wpautop' );


if ( ! function_exists( 'wpt_check_active_plugin' ) ) :
/*
*
* Function to check if Plugin is active or not
*
*/

function wpt_check_active_plugin() {
   if ( function_exists('ai_register_fields' ) ) {
    	echo do_shortcode("[ai_contact_form]");
   }
   else {
   	
   }
}
endif; // wpt_check_active_plugin
add_action( 'plugins_loaded', 'wpt_check_active_plugin' );

require get_template_directory() . '/inc/customizer.php';
