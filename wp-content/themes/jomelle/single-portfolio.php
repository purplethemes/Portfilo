<?php 
get_header();
$portfolio_data = get_post_custom();

get_template_part('partials/header_menu'); 
get_template_part('partials/title_breadcrumbs_bar'); 

global $jomelle_options;

?>

<?php //check if portfolio plugin is activated
if(current_user_can( 'manage_options' ) && !in_array( 'abdev-portfolio/abdev-portfolio.php', get_option( 'active_plugins') )):?>
	<section>
		<div class="container">
			<p>
				<strong><?php _e('This page requires Portfolio plugin to be activated','ABdev_jomelle');?></strong>
			</p>
		</div>
	</section>
<?php endif; ?>

<section>
	<div class="container">
		<?php if (have_posts()) : while (have_posts()) : the_post();?>
			<div class="row">
				<div class="span8 content_with_right_sidebar">
					<?php
						if(isset($portfolio_data['ABp_portfolio_selected_media'][0]) && $portfolio_data['ABp_portfolio_selected_media'][0]=='soundcloud' && isset($portfolio_data['ABp_portfolio_soundcloud'][0]) && $portfolio_data['ABp_portfolio_soundcloud'][0]!=''){
							echo '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F'.$portfolio_data['ABp_portfolio_soundcloud'][0].'"></iframe>';
						}
						elseif(isset($portfolio_data['ABp_portfolio_selected_media'][0]) && $portfolio_data['ABp_portfolio_selected_media'][0]=='youtube' && isset($portfolio_data['ABp_portfolio_youtube_id'][0]) && $portfolio_data['ABp_portfolio_youtube_id'][0]!=''){
							echo '<div class="videoWrapper-youtube"><iframe src="http://www.youtube.com/embed/'.$portfolio_data['ABp_portfolio_youtube_id'][0].'?showinfo=0&amp;autohide=1&amp;related=0" frameborder="0" allowfullscreen></iframe></div>';
						}
						elseif(isset($portfolio_data['ABp_portfolio_selected_media'][0]) && $portfolio_data['ABp_portfolio_selected_media'][0]=='vimeo' && isset($portfolio_data['ABp_portfolio_vimeo_id'][0]) && $portfolio_data['ABp_portfolio_vimeo_id'][0]!=''){
							echo '<div class="videoWrapper-vimeo"><iframe src="http://player.vimeo.com/video/'.$portfolio_data['ABp_portfolio_vimeo_id'][0].'?title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
						}
						elseif(isset($portfolio_data['ABp_portfolio_selected_media'][0]) && $portfolio_data['ABp_portfolio_selected_media'][0]==''){
							the_post_thumbnail('full', array('class' => 'portfolio_item_image'));
						}
						 else {
						 	echo'<div class="slider-wrapper theme-default">
        							   <div id="slider" class="nivoSlider">';
						 	$attachments = get_posts( array(
						 							'post_type' => 'attachment',
						 							'posts_per_page' => -1,
						 							'post_parent' => $post->ID,
						 							'exclude'     => get_post_thumbnail_id()
						 							) );
						 	if ( $attachments ) {
						 		foreach ( $attachments as $attachment ) {
						 			$thumbimg = wp_get_attachment_image_src( $attachment->ID, 'full' );
						 				echo '<img src="'.$thumbimg[0].'" data-thumb="'.$thumbimg[0].'" alt="">';
						 			}
						 		}
						 	echo '</div>
        						</div>';
							
						}
					?>
				</div>
				<div id="portfolio_item_meta" class="span4">
					<h6 class="column_title_left"><?php _e('Description', 'ABdev_jomelle'); ?></h6>
					<?php the_content();?>

					<p class="portfolio_single_detail">
						<span class="portfolio_item_meta_label"><?php _e('Date:', 'ABdev_jomelle');?></span>
						<span class="portfolio_item_meta_data"><?php the_date();?></span>
					</p>

					<?php if(isset($portfolio_data['ABp_portfolio_client'][0])):?>
						<p class="portfolio_single_detail">
							<span class="portfolio_item_meta_label"><?php _e('Client:', 'ABdev_jomelle');?></span>
							<span class="portfolio_item_meta_data"><?php echo $portfolio_data['ABp_portfolio_client'][0];?></span>
						</p>
					<?php endif; ?>

					<p class="portfolio_single_detail">
						<span class="portfolio_item_meta_label"><?php _e('Category:', 'ABdev_jomelle');?></span>
						<span class="portfolio_item_meta_data">
							<?php 
							$terms = get_the_terms( $post->ID , 'portfolio-category' );
							if(is_array($terms)){
								foreach ( $terms as $term ) {
									if(is_object($term)){
										$categories[] = $term->name;
										$related_cat[] = $term->slug;
									}
								} 
							}
							$categories = (isset($categories) && is_array($categories)) ? implode(', ', $categories) : '';
							echo $categories;
							?>
						</span>
					</p>

					<?php if(isset($portfolio_data['ABp_portfolio_link'][0])):?>
						<p class="portfolio_item_view_link"><a href="<?php echo $portfolio_data['ABp_portfolio_link'][0];?>" target="<?php echo $portfolio_data['ABp_portfolio_link_target'][0];?>"><?php _e('Visit site','ABdev_jomelle');?><i class="ci_icon-chevron-right"></i></a></p>
					<?php endif; ?>

					<p class="post_meta_share portfolio_share_social">
						<a class="post_share_facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="ci_icon-facebook"></i></a>
						<a class="post_share_twitter" href="https://twitter.com/home?status=<?php echo(urlencode(__('Check this ', 'ABdev_jomelle'))); ?><?php the_permalink(); ?>"><i class="ci_icon-twitter"></i></a>
						<a class="post_share_googleplus" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><i class="ci_icon-googleplus"></i></a>
						<a class="post_share_linkedin" href="https://www.linkedin.com/shareArticle?mini=<?php the_permalink(); ?>"><i class="ci_icon-linkedin"></i></a>
					</p>

				</div>
			</div>
			
		<?php endwhile; endif;?>
	</div>
</section>


<?php if(isset($portfolio_data['ABp_portfolio_show_related'][0]) && $portfolio_data['ABp_portfolio_show_related'][0]==1): ?>
	<section id="related_portfolio" class="section_body_fullwidth section_no_column_margin section_equalize_5">
		<div class="container">
			<h3 class="column_title_center"><?php _e('Related Projects', 'ABdev_jomelle'); ?></h3>
				<div class="row">
					<?php 
					$related_cat = implode(',', $related_cat);
					$args = array(
						'post_type' => 'portfolio',
						'portfolio-category' => $related_cat,
						'posts_per_page'=>5,
						'post__not_in'=>array($post->ID),
					);
					$related = new WP_Query( $args );
					$out = $error = '';
					if ($related->have_posts()){
						while ($related->have_posts()){
							$related->the_post();
							$slugs=$in_category='';		
							$terms = get_the_terms( $post->ID , 'portfolio-category' );
							if(is_array($terms)){
								foreach ( $terms as $term ) {
									if(is_object($term)){
										$slugs.=' '.$term->slug;
										$filter_slugs[$term->slug] = $term->name;
										$in_category[] = $term->name;
									}
								}
							}

							$in_category = (isset($in_category) && is_array($in_category)) ? implode(', ', $in_category) : '';

							$thumbnail_id = get_post_thumbnail_id($post->ID);
							$thumbnail_object = get_post($thumbnail_id);
							$thumbnail_src=$thumbnail_object->guid;

							echo '<div class="portfolio_item portfolio_item_4 portfolio_fullwidth' . $slugs . '">
								<div class="overlayed">
									' . get_the_post_thumbnail() . '
									<a class="overlay" href="'.get_permalink().'">
										<p class="overlay_title">' . get_the_title() . '</p>
										<p class="portfolio_item_tags">
											'.$in_category.'
										</p>
									</a>
								</div>
							</div>';
						}
					}
					wp_reset_postdata();
					?>
			</div>
		
		</div>
	</section>
<?php endif; ?>

	<?php 
		if(isset($jomelle_options['content_after_portfolio_single']) && $jomelle_options['content_after_portfolio_single']!=''){
			echo do_shortcode($jomelle_options['content_after_portfolio_single']);
		}
	?>

<?php get_footer();