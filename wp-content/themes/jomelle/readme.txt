1.2.1
+ Updated - Drag and Drop Shortcodes to 3.0.2

1.2.0
+ Updated - Drag and Drop Shortcodes to 3.0.1
+ Updated - Redux framework
+ Updated - Revolution Slider to 4.6.0
+ Added - List of all theme icons in Theme options
+ Fix - custom.js google_maps_jquery dependency removed

Version 1.0
- initial release