<?php

/*********** Shortcode: Progress Bar Vertical ************************************************************/

$ABdevDND_shortcodes['progress_bar_vertical_dd'] = array(
	'attributes' => array(
		'choose_height' => array(
			'default' => '150',
			'description' => __('Height of Progress Bar in Pixels', 'dnd-shortcodes'),
		),
		'complete' => array(
			'default' => '60',
			'description' => __('Percentage', 'dnd-shortcodes'),
		),
		'text' => array(
			'description' => __('Text', 'dnd-shortcodes'),
		),
		'bar_color_start' => array(
			'description' => __('Bar Color Start', 'dnd-shortcodes'),
			'type' => 'color',
			'default' => '',
		),
		'bar_color_end' => array(
			'description' => __('Bar Color End', 'dnd-shortcodes'),
			'type' => 'color',
			'default' => '',
		),
		'class' => array(
			'description' => __('Class', 'dnd-shortcodes'),
			'info' => __('Additional custom classes for custom styling', 'dnd-shortcodes'),
		),
	),
	'description' => __('Progress Bar Vertical', 'dnd-shortcodes' )
);
function ABdevDND_progress_bar_vertical_dd_shortcode( $attributes, $content = null ) {
	extract(shortcode_atts(ABdevDND_extract_attributes('progress_bar_vertical_dd'), $attributes));

	$bar_color_out = ($bar_color_start!='') ? 'background:linear-gradient(to top,' .$bar_color_start.',' .$bar_color_end.');' : 'background:' .$bar_color_start.'; ';

	if($bar_color_start!='' && $bar_color_end!=''){
		$bar_color_out = 'background:linear-gradient(to top,' .$bar_color_start.',' .$bar_color_end.');';
	}
	else if($bar_color_start!='' || $bar_color_end!=''){
		$bar_color_out = 'background:' .(($bar_color_start!='') ? $bar_color_start : $bar_color_end).'; ';
	}

	return '
		<div class="dnd_progress_bar_vertical '.$class.'">
			<div class="dnd_meter_vertical" style=" height: '.$choose_height.'px;">
				<div class="dnd_meter_percentage_vertical" data-percentage="'.$complete.'" style=" height: '.$complete.'%;'.$bar_color_out.';"><span>'.$complete.'%</span></div>
			</div>
			<span class="dnd_meter_label">'.$text.'</span>
		</div>';
}

