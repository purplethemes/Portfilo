<?php
get_header();

get_template_part('partials/header_menu'); 
get_template_part('partials/title_breadcrumbs_bar');

global $jomelle_options;

?>
	<section>
		<div class="container">

			<div class="row">

				<div class="span8 content_with_right_sidebar">
					<?php if (have_posts()) :  while (have_posts()) : the_post(); 
						$custom = get_post_custom(); 
						?>
						<div class="post_content">
							<div <?php post_class('post_main'); ?>>
								<?php

									if(isset($custom['ABdevFW_selected_media'][0]) && $custom['ABdevFW_selected_media'][0]=='soundcloud' && isset($custom['ABdevFW_soundcloud'][0]) && $custom['ABdevFW_soundcloud'][0]!=''){
										echo '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F'.$custom['ABdevFW_soundcloud'][0].'"></iframe>';
									}
									elseif(isset($custom['ABdevFW_selected_media'][0]) && $custom['ABdevFW_selected_media'][0]=='youtube' && isset($custom['ABdevFW_youtube_id'][0]) && $custom['ABdevFW_youtube_id'][0]!=''){
										echo '<div class="videoWrapper-youtube"><iframe src="http://www.youtube.com/embed/'.$custom['ABdevFW_youtube_id'][0].'?showinfo=0&amp;autohide=1&amp;related=0" frameborder="0" allowfullscreen></iframe></div>';
									}
									elseif(isset($custom['ABdevFW_selected_media'][0]) && $custom['ABdevFW_selected_media'][0]=='vimeo' && isset($custom['ABdevFW_vimeo_id'][0]) && $custom['ABdevFW_vimeo_id'][0]!=''){
										echo '<div class="videoWrapper-vimeo"><iframe src="http://player.vimeo.com/video/'.$custom['ABdevFW_vimeo_id'][0].'?title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
									}
									else{
										the_post_thumbnail();
									}
									?>
									<h2><a href="<?php the_permalink(); ?>"><?php echo ($post->post_excerpt!=='') ? get_the_excerpt() : get_the_title(); ?></a></h2>
									<div class="post_meta">
										<span class="post_author"><i class="ci_icon-user"></i><?php the_author(); ?></span> 
										<span class="post_date"><i class="ci_icon-time"></i><?php the_date('M j, Y'); ?></span> 
										<span class="post_tags"><i class="ci_icon-tags"></i><?php the_tags( '',', ', ''); ?></span>
										<span class="post_comments"><i class="ci_icon-chat"></i><?php comments_number('','',''); ?></span>
									</div>
								<?php the_content();?>
								
								<?php wp_link_pages('before=<div id="inner_post_pagination" class="clearfix">&after=</div>&link_before=<span>&link_after=</span>'); ?>

								<div class="postmeta-under clearfix">
									<p class="post_meta_tags"><?php the_tags( '<i class="ci_icon-tags"></i>',', ', ''); ?></p>
									<p class="post_meta_share">
										<span><?php _e('Share','ABdev_jomelle'); ?></span>
										<a class="post_share_facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="ci_icon-facebook"></i></a>
										<a class="post_share_twitter" href="https://twitter.com/home?status=<?php echo(urlencode(__('Check this ', 'ABdev_jomelle'))); ?><?php the_permalink(); ?>"><i class="ci_icon-twitter"></i></a>
										<a class="post_share_googleplus" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><i class="ci_icon-googleplus"></i></a>
										<a class="post_share_linkedin" href="https://www.linkedin.com/shareArticle?mini=true&title=<?php urlencode(the_title()); ?>&url=<?php the_permalink(); ?>"><i class="ci_icon-linkedin"></i></a>
									</p>
								</div>

								
								<?php if( !isset($jomelle_options['hide_author_bio']) || (isset($jomelle_options['hide_author_bio']) && $jomelle_options['hide_author_bio'] != '1')):?>
									<div class="post_about_author clearfix">
										<?php echo get_avatar( $post->post_author, 100 ); ?>
										<h6><?php the_author();?></h6> 
										<p><?php echo get_the_author_meta('description'); ?></p>
									</div>
								<?php endif; ?>
								

							</div>
						</div>
							
						
					<?php endwhile; 
					else: ?>
						<p><?php _e('No posts were found. Sorry!', 'ABdev_jomelle'); ?></p>
					<?php endif; ?>
					
					<?php 
					if( isset($jomelle_options['hide_comments']) && $jomelle_options['hide_comments'] != '1'):?>
						<section id="comments_section" class="section_border_top">
							<?php comments_template('/partials/comments.php'); ?> 
						</section>
					<?php endif; ?>

				</div><!-- end span8 main-content -->
				
				<aside class="span4 sidebar sidebar_right">
					<?php 
					if(isset($custom['custom_sidebar'][0]) && $custom['custom_sidebar'][0]!=''){
						$selected_sidebar=$custom['custom_sidebar'][0];
					}
					else{
						$selected_sidebar=__( 'Primary Sidebar', 'ABdev_jomelle');
					}
					dynamic_sidebar($selected_sidebar);
					?>
				</aside><!-- end span4 sidebar -->

			</div><!-- end row -->

		</div>
	</section>


<?php get_footer();