<?php

$testimonial_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'testimonial-homepage');
$testimonial_imgURL = $testimonial_imgArray[0];
?>
	<article class="slide">
		<div class="testimonial-image">
			<div class="bubble-image">
		
		    	<?php if(!empty($testimonial_imgURL)) { ?> <img alt="black" class="attachment-small wp-post-image" src="<?php echo $testimonial_imgURL ?>" title="exp" />
		    	<?php } else { ?> <img alt="black" class="attachment-small wp-post-image" src="<?php echo TESTI_PLUGIN_URL. '/images/no-img-portfolio.jpg' ?>" style="width:87px;height:88px;" title="exp" />
		    	<?php } ?>
				<div class="substrate">
                    <img alt="" src="<?php echo TESTI_PLUGIN_URL. '/images/testimonial_bg.png' ?>">
            	</div>
		    </div>
		</div> 
		               		
		<div class="testimonials-carousel-context">
		    <div class="testimonials-carousel-content">
		        <p><?php the_content(); ?></p>
		        <div class="testimonials-name">-<?php the_title(); ?></div>
		    </div>
		</div>
		
	</article>
	
	