var $container;
$(function () {
    
    "use strict";
    
    /* Sticky Header */
//    $(window).scroll(function () {
//        if ($(window).scrollTop() > 0) {
//            $('header').css('position', 'fixed');
//            $('#logo').css('padding', '1px 10px');
//            $('#main_navigation ul.navbar-nav > li > a').css('padding', '10px');
//            $('.navbar-toggle').css('margin', '4px 0');
//            $('.navbar-collapse').css('top', '0px');
//            
//        } else {
//            $('header').css('position', 'relative');
//            $('#logo').css('padding', '9px 10px');
//            $('#main_navigation ul.navbar-nav > li > a').css('padding', '18px 10px');
//            $('.navbar-toggle').css('margin', '11px 0');
//            $('.navbar-collapse').css('top', '2px');
//        }
//    });

    /* Body Vertical Scrollbar */
    $("html").niceScroll({
        //autohidemode: false
    }); 
    
    /* Tooltip */
    $('.ir').tooltip();

    /* Counter */
    $('.cnt-no').counterUp({
        delay: 100,
        time: 2000
    });

    /* ---------------------------------------------------------------------------
	 * Main menu
	 * --------------------------------------------------------------------------- */
	$('#menu > ul').aigroup_menu({
		delay: 0,
		hoverClass: 'hover',
		arrows: true,
		animation: ''
	});

    /* Inquiry */
    $('.inquiry').click(function () {
        $('.inquiry').toggleClass('active_inquiry');
        $('.inquiery-form').slideToggle("slow", function () {
            return $('.inquiery-form').is(":visible");
        });
    });
    $('#close_form').click(function () {
        $('.inquiry').removeClass('active_inquiry');
        $('.inquiery-form').slideToggle("slow", function () {
            return $('.inquiery-form').is(":visible");
        });

    });
    /* Form Validation */
    $('#inqiry').validate();
    $('#newsletter_form').validate({
    							
		submitHandler: function(form) {
			$(form).ajaxSubmit({
				url:Wpt_Ajax,
				type: "POST",			
				dataType: "json",
				data: { action: 'ai_newsletter' },
				success:function(response) {
					if(response == 1) {
						jQuery("#newsletter_form").append('You have sucessfully subscribe to our newsletter.');
						document.getElementById('newsletter_form').reset();							
					}	
				}
			});	
		}			
	});
    $('.contact_form').validate();
    $('#validate').validate();
    /* Project Detail Page ( Project Overview tab's button ) */
    $('.view-plan').click(function () {
        $('#project_detail_tab li').removeClass('active');
        $('#project_detail_tab_content .tab-pane').removeClass('active');
        $('#project_detail_tab li.project-plan').addClass('active');
        showtab = $('#project_detail_tab li.project-plan').find('a').attr('href');
        $(showtab).addClass('active in');
    });
    $('.view-gallary').click(function () {
        $('#project_detail_tab li').removeClass('active');
        $('#project_detail_tab_content .tab-pane').removeClass('active');
        $('#project_detail_tab li.gallary').addClass('active');
        showtab = $('#project_detail_tab li.gallary').find('a').attr('href');
        $(showtab).addClass('active in');
    });


    /* Search */

    $("#menu .btn_search").click(function (e) {
        e.stopPropagation();
        $('#menu #search_text').stop(true, true).animate({
            width: 'toggle'
        }, 400).focus();
        $("#menu .btn_search").toggleClass("active");
    });
    $("body").click(function (e) {
        //e.stopPropagation();
        $('#menu #search_text').stop(true, true).animate({
            width: 'hide'
        }, 400).focus();
    });
    $('#menu #search_text').click(function (e) {
        e.stopPropagation();
        $(this).stop(true, true).animate({
            width: 'show'
        }, 400);
    });

    /* Project Detail Page ( Map on Project location Tab ) */
    $("#project_detail_tab li").click(function () {
        map_id = $(this).attr('class');

        if (map_id.length > 0) {
            initialize(map_id);
        }
    });
    $(".mappopup").click(function () {
        initialize('project-neighbour-map-model');
    });

    /* Scroll to top */
    var offset = 220;
    var duration = 1000;
    $(window).scroll(function () {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(duration);
        } else {
            $('.back-to-top').fadeOut(duration);
        }
    });
    $('.back-to-top').click(function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, duration);
        return false;
    });




    /* Project Detail Page selected tab display in url */
    var hash = window.location.hash;
    if (hash != "")
        $('#project_detail_tab a[href="' + hash + '"]').tab('show');
    else
        $('#project_detail_tab a').eq(1).tab('show');


    // For place holder

    var _debug = false;
    var _placeholderSupport = function () {
        var t = document.createElement("input");
        t.type = "text";
        return (typeof t.placeholder !== "undefined");
    }();

    window.onload = function () {
        var arrInputs = document.getElementsByTagName("input");
        var arrTextareas = document.getElementsByTagName("textarea");
        var combinedArray = [];
        for (var i = 0; i < arrInputs.length; i++)
            combinedArray.push(arrInputs[i]);
        for (var i = 0; i < arrTextareas.length; i++)
            combinedArray.push(arrTextareas[i]);
        for (var i = 0; i < combinedArray.length; i++) {
            var curInput = combinedArray[i];
            if (!curInput.type || curInput.type == "" || curInput.type == "text" || curInput.type == "textarea")
                HandlePlaceholder(curInput);
            else if (curInput.type == "password")
                ReplaceWithText(curInput);
        }

        if (!_placeholderSupport) {
            for (var i = 0; i < document.forms.length; i++) {
                var oForm = document.forms[i];
                if (oForm.attachEvent) {
                    oForm.attachEvent("onsubmit", function () {
                        PlaceholderFormSubmit(oForm);
                    });
                } else if (oForm.addEventListener)
                    oForm.addEventListener("submit", function () {
                        PlaceholderFormSubmit(oForm);
                    }, false);
            }
        }
    };

    function PlaceholderFormSubmit(oForm) {
        for (var i = 0; i < oForm.elements.length; i++) {
            var curElement = oForm.elements[i];
            HandlePlaceholderItemSubmit(curElement);
        }
    }

    function HandlePlaceholderItemSubmit(element) {
        if (element.name) {
            var curPlaceholder = element.getAttribute("placeholder");
            if (curPlaceholder && curPlaceholder.length > 0 && element.value === curPlaceholder) {
                element.value = "";
                window.setTimeout(function () {
                    element.value = curPlaceholder;
                }, 100);
            }
        }
    }

    function ReplaceWithText(oPasswordTextbox) {
        if (_placeholderSupport)
            return;
        var oTextbox = document.createElement("input");
        oTextbox.type = "text";
        oTextbox.id = oPasswordTextbox.id;
        oTextbox.name = oPasswordTextbox.name;
        //oTextbox.style = oPasswordTextbox.style;
        oTextbox.className = oPasswordTextbox.className;
        for (var i = 0; i < oPasswordTextbox.attributes.length; i++) {
            var curName = oPasswordTextbox.attributes.item(i).nodeName;
            var curValue = oPasswordTextbox.attributes.item(i).nodeValue;
            if (curName !== "type" && curName !== "name") {
                oTextbox.setAttribute(curName, curValue);
            }
        }
        oTextbox.originalTextbox = oPasswordTextbox;
        oPasswordTextbox.parentNode.replaceChild(oTextbox, oPasswordTextbox);
        HandlePlaceholder(oTextbox);
        if (!_placeholderSupport) {
            oPasswordTextbox.onblur = function () {
                if (this.dummyTextbox && this.value.length === 0) {
                    this.parentNode.replaceChild(this.dummyTextbox, this);
                }
            };
        }
    }

    function HandlePlaceholder(oTextbox) {
        if (!_placeholderSupport) {
            var curPlaceholder = oTextbox.getAttribute("placeholder");
            if (curPlaceholder && curPlaceholder.length > 0) {
                Debug("Placeholder found for input box '" + oTextbox.name + "': " + curPlaceholder);
                oTextbox.value = curPlaceholder;
                oTextbox.setAttribute("old_color", oTextbox.style.color);
                oTextbox.style.color = "#c0c0c0";
                oTextbox.onfocus = function () {
                    var _this = this;
                    if (this.originalTextbox) {
                        _this = this.originalTextbox;
                        _this.dummyTextbox = this;
                        this.parentNode.replaceChild(this.originalTextbox, this);
                        _this.focus();
                    }
                    Debug("input box '" + _this.name + "' focus");
                    _this.style.color = _this.getAttribute("old_color");
                    if (_this.value === curPlaceholder)
                        _this.value = "";
                };
                oTextbox.onblur = function () {
                    var _this = this;
                    Debug("input box '" + _this.name + "' blur");
                    if (_this.value === "") {
                        _this.style.color = "#c0c0c0";
                        _this.value = curPlaceholder;
                    }
                };
            } else {
                Debug("input box '" + oTextbox.name + "' does not have placeholder attribute");
            }
        } else {
            Debug("browser has native support for placeholder");
        }
    }

    function Debug(msg) {
        if (typeof _debug !== "undefined" && _debug) {
            var oConsole = document.getElementById("Console");
            if (!oConsole) {
                oConsole = document.createElement("div");
                oConsole.id = "Console";
                document.body.appendChild(oConsole);
            }
            oConsole.innerHTML += msg + "<br />";
        }
    }

});

$(window).load(function () {
    
    "use strict";
    
    /* Home page Slider */
    $('.slider-banner').flexslider({
        animation: "fade",
        directionNav: true,
        slideshow: true,
        slideshowSpeed: 5000,
        animationSpeed: 1500,

        after: function (slider) {
            if (!slider.playing) {
                slider.play();
            }
        }
    });



});

function isMobile() {
    
    "use strict";
    
    return (
        (navigator.userAgent.match(/Android/i)) ||
        (navigator.userAgent.match(/webOS/i)) ||
        (navigator.userAgent.match(/iPhone/i)) ||
        (navigator.userAgent.match(/iPod/i)) ||
        (navigator.userAgent.match(/iPad/i)) ||
        (navigator.userAgent.match(/BlackBerry/))
    );
}
/* For Animation */
try {

    wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });
    wow.init();
} catch (e) {};

// For map on project-detail page
function initialize(map_id) {
    
    "use strict";
    
    //var image = 'images/map-marker.png';
    setTimeout(function () {
        var mapOptions = {
            zoom: 13,
            center: new google.maps.LatLng(43.639245, -79.535783),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById(map_id), mapOptions);
        var myPos = new google.maps.LatLng(43.639245, -79.535783);
        var myMarker = new google.maps.Marker({
            position: myPos,
            map: map,
            draggable: false
        });

        var infowindow = new google.maps.InfoWindow({
            content: ''
        });

        bindInfoWindow(myMarker, map, infowindow, "15 Viking Lane,Suite 2610,<br/>Toronto M9B 0A4,<br/>ON, Canada.");
    }, 500);
}

function bindInfoWindow(myMarker, map, infowindow, html) {
    
    "use strict";
    
    infowindow.setContent(html);
    infowindow.open(map, myMarker);
}

// for devices hover
$(function () {
    
    "use strict";
    
    var mobileHover = function () {
        /* For lightbox popup open in devices */
        $('.pro-img').on('touchstart', function () {
            $(this).trigger('focus');
        }).on('touchend', function () {
            $(this).trigger('focus');
        });
        $('.gallery-img').on('touchstart', function () {
            $(this).trigger('hover');
        }).on('touchend', function () {
            $(this).trigger('hover');
        });

        $('.plan-img').on('touchstart', function () {
            $(this).trigger('focus');
        }).on('touchend', function () {
            $(this).trigger('focus');
        });

        /* For Project Detail Page Tab-collaspe */
        $('#project_detail_tab li a').on('click', function () {
            window.location.hash = $(this).attr('href');
        });
        $('#project_detail_tab-accordion .panel-title a').on('click', function () {
            window.location.hash = $(this).attr('href');
        });
    };

    mobileHover();
});

function chk_null() {
    
    "use strict";
    
    if ($('#menu #search_text').val() == '') {
        //$("#menu .btn_search").show();
        //$("#menu #search_submit").show();
        //alert($('#menu #search_text').val());
        e.stopPropagation();
        $('#menu #search_text').stop(true, true).animate({
            width: 'toggle'
        }, 400).focus();
    } else if ($('#menu #search_text').val() != '') {
        //alert("hello");
        $("#menu .btn_search").hide();
        $("#menu #search_submit").show();
        e.stopPropagation();
        $('#menu #search_text').stop(true, true).animate({
            width: 'hide'
        }, 400).focus();
    }
} 
/* Testimonial */
$(document).ready(function () {
            
            $('.testimonials-slider').bxSlider({
                //slideWidth: 900,
                minSlides: 1,
                maxSlides: 1,
                auto: true,
                mode: 'fade',
                adaptiveHeight: true
            });
        });
