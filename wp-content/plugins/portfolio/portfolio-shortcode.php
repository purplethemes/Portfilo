<?php
add_shortcode( 'portfolio', 'portfolioShortcode' );
function portfolioShortcode( $attr, $content = null )
{
   wp_head(); 
   extract(shortcode_atts(array(
		'count' => '4',
		'category' => '',
		'orderby' => 'menu_order',
		'order' => 'ASC',
	), $attr));
	    
	$args = array( 
		'post_type' => 'portfolio',
		'posts_per_page' => intval($count),
		'paged' => -1,
        'post_status'=> 'publish',
		'orderby' => $orderby,
		'order' => $order,	
		'ignore_sticky_posts' =>1,
	);
    if( $category ) $args['portfolio-types'] = $category;
    
    $query = new WP_Query( $args );
    if ($query->have_posts())
	{ 
        $portfolio_title = get_option('portfolio_title');
         if( empty( $portfolio_title ) )
           $portfolio_title = 'Our recent works';
           
	    $portfolio_content = get_option( 'portfolio_content' );
	    
        $html = '';
		$html .='<section class="recent_project">';		
		$html .='<div class="title-area wow  fadeIn">';	
		$html .='<h2 class="section-title">'. $portfolio_title. '</h2>';
		$html .='<div class="section-divider divider-inside-top"></div>';
		$html .='<p class="section-sub-text">'.$portfolio_content. '</p>';
        $html .='<article class="project-list">';
        $html .='<section class="row">';
        
        while ($query->have_posts()) 
		{
                    $query->the_post();
                    $term_count = '';
                    $portfolio_terms_deatils = get_the_terms( get_the_ID(), 'portfolio_category' );
                    
                    $term_count = count($portfolio_terms_deatils);
                   	$pf_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'portfolio-homepage');
                    $large_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'portfolio-homepagelarge');
                    $pf_imgURL = $pf_imgArray[0];
                    $large_imgURL = $large_imgArray[0];
                    $full_title =  get_the_title();
                    $html .='<article class="col-xs-6 col-sm-3 col-md-3">';
                    $html .='<div class="project-container wow fadeInLeft" data-wow-delay="0.4s">';
                    $html .='<div class="pro-img">';
                    
                    if( !empty( $pf_imgURL ) ) $html .='<img src="' .$pf_imgURL. '" class="img-responsive" alt="Project Name" title="Project Name" />';
					else $html .='<img src="' .WPT_PLUGIN_URL. '/images/no-img-portfolio.jpg'. '" style="width:253px;height:253px;" class="img-responsive" alt="Project Name" title="Project Name" />';
					
                     if( !empty( $large_imgURL ) ) $html .='<div class="portfolio-entry-hover"> <a class="gallery" href="' .$large_imgURL. '" data-title="'.$full_title.'" data-lightbox="image-3"><i class="glyphicon glyphicon-zoom-in"></i></a>';
                   
                    else $html .='<div class="portfolio-entry-hover"> <a class="gallery" href="' .WPT_PLUGIN_URL. '/images/no-img-portfolio.jpg'. '" data-title="'.$full_title.'" data-lightbox="image-3"><i class="glyphicon glyphicon-zoom-in"></i></a>';
                   
                    $html .='</div>';
                    $html .='</div>';
                    $html .='<h4><a href="'. get_permalink() .'">' .the_title(false, false, false). '</a></h4>';
                    
                    if( $portfolio_terms_deatils ){
						$html .='<span>'; 
						foreach( $portfolio_terms_deatils as $category ){
							 $html .='<a href="'. get_term_link( $category ) .'">' .$category->name. '</a>';
							 if( $term_count > 1 && $term_count != '1' ){
							 	  $html .= ',';
							 	  $term_count--;
							 } 	  
						}
						$html .='</span>'; 
					}
                    $html .='</div>';
                    $html .='</article>';
				} 
		$html .='</section>';
        $html .='</article>';
        $html .='</div>';
	    $html .='</section>';	  
	}
	wp_reset_query();	
    return $html;
}
?>